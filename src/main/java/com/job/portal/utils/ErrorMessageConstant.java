package com.job.portal.utils;

public class ErrorMessageConstant {

	
	
	public static final String ROLE_NOT_FOUND ="Role not found";
	public static final String INVALID_INFORMATION = "Invalid email or password";
	public static final String FAILED_TO_SEND_OTP = "Failed to send otp";
	
	
	public static final String INVALID_PASSWORD_FORMAT = "Password should have Minimum 8 and "
			+ "maximum 50 characters,at least one uppercase letter, one lowercase letter, "
			+ "one number and one special character and No White Spaces";
}
