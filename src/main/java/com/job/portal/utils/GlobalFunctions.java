package com.job.portal.utils;

import org.springframework.data.domain.Pageable;

public class GlobalFunctions {

	
	public static final String CUSTOM_USER_ID ="X-user-id";
	public static final String CUSTOM_USER_EMAIL="X-user-email";
	
	
	public static Pageable getPagination(String pageNo, String pageSize) {

		String pageNumber = pageNo.isBlank() ? Constant.DEFAULT_PAGENUMBER : pageNo ;
		String pages = pageSize.isBlank() ? Constant.DEFAULT_PAGESIZE : pageSize ;
		
		Pageable pageable = new Pagination().getPagination(pageNumber, pages);
		
		if(pageNo.isBlank() && pageSize.isBlank()) {
			pageable = Pageable.unpaged();
		}
		return pageable;
	}
	
}
