package com.job.portal.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
	
	
	public static final String PASSWORD_PATTERN = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@#$%^&+=!])(?=.*[a-zA-Z@#$%^&+=!\\d]).{8,}$";
	
	public static boolean validatePassword(String password) {
		Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
		Matcher matcher = pattern.matcher(password);
		return matcher.matches();
	}

	
	
	 

}
