package com.job.portal.utils;

public class Constant {

	
	public static final String PAGENUMBER = "pageNo";
	public static final String PAGESIZE = "pageSize";
	
	
	public static final String DEFAULT_PAGENUMBER = "1";
	public static final String DEFAULT_PAGESIZE = "10";
	
	public static final String TYPE = "type"	;
	public static final String TOKEN_TYPE_ACCESS = "access";
	public static final String TOKEN_TYPE_REFRESH = "refresh";
	public static final String AUTHORIZATION = "Authorization";
	public static final String BEARER ="Bearer ";
	
	
	
}
