package com.job.portal.utils;

public class ApiUrls {
	
	public static final String API = "/api";
	public static final String AUTH = "/auth";
	public static final String ROLE ="/role";
	public static final String REGISTER = "/register";
	public static final String LOGIN = "/login";
	public static final String GENERATE_OTP = "/generate-otp";
	public static final String FORGOT_PASSWORD_CONFIRM ="/forgot-password/confirm";

	
	
	public static final String[] SWAGGER_URLS = { "/v3/api-docs", "/v2/api-docs", "/v3/api-docs/**", "/swagger-resources","/swagger-resources/**",
			"/swagger-ui/**", "/webjars/**", "/api/swagger-ui/index.html" ,"/swagger-ui.html"};
	
	
	
	public static final String[] URLS_WITHOUT_HEADER = {
			ApiUrls.API + ApiUrls.AUTH + ApiUrls.LOGIN , 
			ApiUrls.API + ApiUrls.AUTH +ApiUrls.REGISTER ,
			ApiUrls.API + ApiUrls.AUTH + GENERATE_OTP, 
			ApiUrls.API + ApiUrls.AUTH + FORGOT_PASSWORD_CONFIRM
	};


}

