package com.job.portal.utils;

public class SuccessMessageConstant {

	
	
	public static final String ROLE_ADDED= "Role added successfully";
	public static final String OTP_SENT = "Otp sent successfully";
	
	public static final String DATA_FETCH_SUCCESSFULLY = "data fetch successfully.";
	public static final String USER_UPDATED_SUCCESSFULLY = "User updated successfully.";
	
	
}
