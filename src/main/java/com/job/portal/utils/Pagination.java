package com.job.portal.utils;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public class Pagination {

	public Pagination() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public Pageable getPagination(String PageNumber , String PageSize) {
		return PageRequest.of(Integer.parseInt(PageNumber)-1, Integer.parseInt(PageSize));
	}
}
