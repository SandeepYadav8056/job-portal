package com.job.portal.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.time.LocalDate;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.job.portal.enums.Gender;
import com.job.portal.idtos.ICandidateJobExportDto;
import com.job.portal.idtos.IUserListDto;

public class ExportExcelFileUtil {
	
	
	public static final String[] HEADERS_USER = {
			
			"Sr.NO",
			"NAME",
			"EMAIL",
			"GENDER"
			
	};
	
	
	public static final String[] HEADERS_CANDIDATE_JOB = {
			"SR.NO",
			"NAME",
			"EMAIL",
			"GENDER",
			"JOB TITLE",
			"APPLICATION DATE",
			"STATUS"
	};
 
	
	public static ByteArrayInputStream exportUserToExcel(List<IUserListDto> lists,String sheetName) throws Exception {
		
		Workbook workbook =null; ;
		ByteArrayOutputStream out= null;
		try {
			
			//create workbook
			
			 workbook = new XSSFWorkbook();
			 out = new ByteArrayOutputStream();
			
			//create sheet
			Sheet sheet = workbook.createSheet(sheetName);
	
			//create row 
			
			Row row  = sheet.createRow(0);
			
			for(int i =0;i<HEADERS_USER.length;i++) {
				
				Cell cell = row.createCell(i);
				cell.setCellValue(HEADERS_USER[i]);
				
			}
			
			//values row
			int rowIndex=1;
			
			for(IUserListDto user : lists) {
				
				Row dataRow = sheet.createRow(rowIndex);
				rowIndex++;
				
				dataRow.createCell(0).setCellValue(rowIndex);
				dataRow.createCell(1).setCellValue(user.getName());
				dataRow.createCell(2).setCellValue(user.getEmail());
				dataRow.createCell(3).setCellValue( user.getGender().toString());
				
			}
			workbook.write(out);
			return new ByteArrayInputStream(out.toByteArray());
			
		}catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}finally {
			workbook.close();
			out.close();
		}
	}
	
	
	
	public static ByteArrayInputStream exportCandidateJobToExcel(List<ICandidateJobExportDto> list) throws Exception {
		
		
		
		Workbook workbook = null;
		
		ByteArrayOutputStream out =null;
		

		try {
			
			
			//create workbook
			
			 workbook = new XSSFWorkbook();
			
			 out = new ByteArrayOutputStream();
			
			
			
			//create sheet 
			Sheet sheet = workbook.createSheet("candidateJobDetails");
			
			
			 Row row = sheet.createRow(0);
			 
			 for(int i =0;i<HEADERS_CANDIDATE_JOB.length;i++) {
				 
				 Cell cell =row.createCell(i);
				 cell.setCellValue(HEADERS_CANDIDATE_JOB[i]);
			 }
			 
			 
			 int rowIndex = 1;
			 
			 for(ICandidateJobExportDto data : list) {
				 
				 Row rowData = sheet.createRow(rowIndex);
				 
				 
				 rowData.createCell(0).setCellValue(rowIndex);
				 rowData.createCell(1).setCellValue(data.getName());
				 rowData.createCell(2).setCellValue(data.getEmail());
				 rowData.createCell(3).setCellValue(data.getGender().toString());
				 rowData.createCell(4).setCellValue(data.getJobTitle());
				 rowData.createCell(5).setCellValue(data.getApplicationDate().toString());
				 rowData.createCell(6).setCellValue(data.getStatus().toString());
				 rowIndex++;
					
	
			 }
			 workbook.write(out);
			 
			
			 return new ByteArrayInputStream(out.toByteArray());
			
			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
			
		}finally {
			workbook.close();
			out.close();
		}
		
		
		
	}
	
	
	  
	
}
