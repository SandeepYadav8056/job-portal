package com.job.portal.dtos;

public class PaginationResponse {

	
	private Integer pageNo;
	private Integer pageSize;
	private Long total;
	
	
	public PaginationResponse() {
		super();
		// TODO Auto-generated constructor stub
	}


	public PaginationResponse(Integer pageNo, Integer pageSize, Long total) {
		super();
		this.pageNo = pageNo;
		this.pageSize = pageSize;
		this.total = total;
	}


	public Integer getPageNo() {
		return pageNo;
	}


	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}


	public Integer getPageSize() {
		return pageSize;
	}


	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}


	public Long getTotal() {
		return total;
	}


	public void setTotal(Long total) {
		this.total = total;
	}
	
	
	
	
}
