package com.job.portal.dtos;

public class ListResponseDto {
	
	private String message;
	
	private Object data;
	
	private Object count;

	public ListResponseDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ListResponseDto(String message,Object data, Object count) {
		super();
		this.message = message;
		this.data = data;
		this.count = count;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Object getCount() {
		return count;
	}

	public void setCount(Object count) {
		this.count = count;
	}
	
	public String getMessage() {
		return this.message;
	}
	
	public void setMessage(String message) {
		this.message=message;
	}
	
}
