package com.job.portal.dtos;

import java.util.List;

import org.json.JSONObject;

public class AuthResponseDto {

	private String accessToken;
	
	private String refreshToken;
	
	private List<String> permissions;

	public AuthResponseDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AuthResponseDto(String accessToken, String refreshToken, List<String> permissions) {
		super();
		this.accessToken = accessToken;
		this.refreshToken = refreshToken;
		this.permissions = permissions;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public List<String> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<String> permissions) {
		this.permissions = permissions;
	}
	
	
	@Override
	public String toString() {

// Create a new JSON object
		JSONObject jsonObject = new JSONObject();

// Add the key-value pairs to the JSON object
		jsonObject.put("accessToken", accessToken);
		jsonObject.put("refreshToken", refreshToken);
		jsonObject.put("permission", permissions);
		
// Convert the JSON object to a string
		return jsonObject.toString();
	}

	
	
}
