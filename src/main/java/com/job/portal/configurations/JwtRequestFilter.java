package com.job.portal.configurations;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.job.portal.entities.UserMaster;
import com.job.portal.repositories.UserMasterRepository;
import com.job.portal.service.AuthService;
import com.job.portal.utils.Constant;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
public class JwtRequestFilter extends OncePerRequestFilter  {

	@Autowired
	private JwtTokenUtils jwtTokenUtils;

	@Autowired
	private AuthService authService;
	
	@Autowired
	private UserMasterRepository userRepository;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		final String authHeader = request.getHeader(Constant.AUTHORIZATION);

		MultiReadHttpServletRequest httpServletRequest = new MultiReadHttpServletRequest(request);

		String userEmail = null;
		String jwtToken = null;

		try {

			if (authHeader != null && authHeader.startsWith(Constant.BEARER)
					&& jwtTokenUtils.getTokenType(authHeader.substring(7)).equals(Constant.TOKEN_TYPE_ACCESS)) {

				jwtToken = authHeader.substring(7);
				userEmail = jwtTokenUtils.getUsernameFromToken(jwtToken);
			}

		} catch (ExpiredJwtException e) {
			response.setHeader("Error", "Jwt Expired");
		} catch (MalformedJwtException e) {
			response.setHeader("Error", "Malformed Jwt");
		} catch (Exception e) {
			logger.error(e.getMessage());
		} 

		if (userEmail != null && SecurityContextHolder.getContext().getAuthentication() == null) {
			UserDetails userDetails = this.authService.loadUserByUsername(userEmail);
			if (jwtTokenUtils.validateToken(jwtToken, userDetails)) {
				UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
						userDetails, null, userDetails.getAuthorities());
				usernamePasswordAuthenticationToken
						.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
			}

		}
		
		if(null != jwtToken) {
			final String emailString = jwtTokenUtils.getUsernameFromToken(jwtToken);
			
			UserMaster userMaster=  userRepository.findByEmailIgnoreCaseAndIsActiveTrue(emailString).get();
			request.setAttribute("X-user-id", userMaster.getId());
			request.setAttribute("X-user-email", userMaster.getEmail());
		}
		
		filterChain.doFilter(httpServletRequest, response);

	}

}
