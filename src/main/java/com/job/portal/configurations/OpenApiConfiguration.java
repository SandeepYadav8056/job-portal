package com.job.portal.configurations;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.servers.Server;

@OpenAPIDefinition(

			info = @Info(
					contact = @Contact(
							name = "Sandeep",
							email = "deep@gmail.com"
							),
					description = "OpenApi documentation for Job Portal",
					title = "Job Portal",
					version = "1.0",
					license = @License(
							
							name = "License"
							
							),
					termsOfService = "Terms of services"
					
					) , 
			
			servers = {
					
				@Server(
					
					description = "Local ENV",
					url = "http://localhost:8081/api"
					
					),
			
				@Server(
					description = "PROD ENV",
					url = "https://jobportal.com"
						
						)
			
			}
			,
			security = {
					@SecurityRequirement(
							name="bearerAuth"
							)
			}
		
)
@SecurityScheme(
		name = "bearerAuth",
		description = "JWT auth description",
		scheme = "bearer",
		type = SecuritySchemeType.HTTP,
		bearerFormat = "JWT",
		in = SecuritySchemeIn.HEADER
		
		)

public class OpenApiConfiguration {

	
}
