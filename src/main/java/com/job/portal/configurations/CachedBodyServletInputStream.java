package com.job.portal.configurations;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import jakarta.servlet.ReadListener;
import jakarta.servlet.ServletInputStream;


public class CachedBodyServletInputStream  extends ServletInputStream{
	
	
	private InputStream cachedBodyInputStream;
	
	public CachedBodyServletInputStream(byte[] cachedBody) {
		super();
		this.cachedBodyInputStream = new ByteArrayInputStream(cachedBody);
	}


	@Override
	public boolean isFinished() {
		
		try {
			return cachedBodyInputStream.available() ==0;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean isReady() {
		
		return true;
	}

	@Override
	public void setReadListener(ReadListener listener) {
		throw new  UnsupportedOperationException();
	}

	@Override
	public int read() throws IOException {
		return cachedBodyInputStream.read();
	}

}
