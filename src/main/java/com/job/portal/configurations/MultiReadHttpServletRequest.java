package com.job.portal.configurations;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.springframework.util.StreamUtils;

import jakarta.servlet.ServletInputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletRequestWrapper;

public class MultiReadHttpServletRequest extends HttpServletRequestWrapper{


	private byte[] cacheBody;
	
	
	public MultiReadHttpServletRequest(HttpServletRequest request) throws IOException {
		super(request);
	
		
		if((request.getContentType() == null) || (request.getContentType().toLowerCase().indexOf("multipart/form-data")) <= -1) {
			InputStream inputStream = request.getInputStream();
			this.cacheBody= StreamUtils.copyToByteArray(inputStream);
		}
	}
	
	@Override 
	public ServletInputStream getInputStream() throws IOException {
		return new CachedBodyServletInputStream(cacheBody);
	}
	
	@Override
	public BufferedReader getReader() throws IOException{
		ByteArrayInputStream arrayInputStream = new ByteArrayInputStream(cacheBody);
		return new BufferedReader(new InputStreamReader(arrayInputStream));
	}

}
