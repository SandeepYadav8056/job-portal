package com.job.portal.configurations;

import java.io.Serializable;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.job.portal.service.JwtTokenService;
import com.job.portal.utils.Constant;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

@Component
public class JwtTokenUtils implements JwtTokenService ,Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Long JWT_TOKEN_VALADITY_FOR_ACCESS_TOKEN =86400L;  // 60*60*24 for 1 day
	
	private static final Long JWT_TOKEN_VALADITY_FOR_REFRESH_TOKEN =604800L; //60*60*24*7 7 days 60*15 for minute
	
	@Value("${jwt.secretKey}")
	private String secretKey;
	

	

	@Override
	public String getUsernameFromToken(String token) {
		 return getClaimsFromToken(token, Claims::getSubject);
	}

	@Override
	public Date getExpirationDateFromToken(String token) {
		
		return getClaimsFromToken(token, Claims::getExpiration);
	}

	@Override
	public Claims getAllClaimsFromToken(String token) {
		return Jwts.parserBuilder().setSigningKey(getSignInKey()).build().parseClaimsJws(token).getBody();
	}

	private Key getSignInKey() {
		byte[] keyBytes = Decoders.BASE64.decode(secretKey);
		return Keys.hmacShaKeyFor(keyBytes);
	}

	@Override
	public Boolean isTokenExpired(String token) {
		final Date expiration = getExpirationDateFromToken(token);
		return expiration.before(new Date());
	}

	@Override
	public String generateToken(UserDetails userDetails) {
		Map<String, Object> claims = new HashMap<>();
		return doGenerateToken(claims,userDetails.getUsername());
	}

	private String doGenerateToken(Map<String, Object> claims, String username) {
		
		return Jwts.builder().setClaims(claims).claim(Constant.TYPE, Constant.TOKEN_TYPE_ACCESS).setSubject(username)
		.setIssuedAt(new Date(System.currentTimeMillis())).setExpiration(new Date(System.currentTimeMillis() +1000*JWT_TOKEN_VALADITY_FOR_ACCESS_TOKEN))
		.signWith(getSignInKey(),SignatureAlgorithm.HS512).compact();
		
	
	}

	@Override
	public Boolean validateToken(String token, UserDetails userDetails) {
		final String username = getUsernameFromToken(token);
		return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
		}

	@Override
	public String refreshToken(String token, UserDetails userDetails) {
	
		final Date createdDate = new Date();
		final Date expirationDate =calculateExpirationDate(createdDate);
		
		final Claims claims = getAllClaimsFromToken(token);
		claims.setIssuedAt(createdDate);
		claims.setExpiration(expirationDate);
		
		return Jwts.builder().setClaims(claims).claim(Constant.TYPE, Constant.TOKEN_TYPE_REFRESH).setSubject(userDetails.getUsername())
				.signWith(getSignInKey(), SignatureAlgorithm.HS512).compact();
		
	}

	@Override
	public Date calculateExpirationDate(Date createdDate) {
		return new Date(createdDate.getTime() + 1000 *JWT_TOKEN_VALADITY_FOR_REFRESH_TOKEN);
	}

	@Override
	public Boolean canTokenBeRefreshed(String token) {
		return (!isTokenExpired(token) || ignoreTokenExpiration(token));
	}

	private boolean ignoreTokenExpiration(String token) {
		return false;
	}

	@Override
	public String getTokenType(String token) throws Exception {
		Claims claims=  Jwts.parserBuilder().setSigningKey(getSignInKey()).build().parseClaimsJws(token).getBody();
		return (String)claims.get(Constant.TYPE);
	}

	@Override
	public <T> T getClaimsFromToken(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = getAllClaimsFromToken(token);
		return claimsResolver.apply(claims);
	}

}
