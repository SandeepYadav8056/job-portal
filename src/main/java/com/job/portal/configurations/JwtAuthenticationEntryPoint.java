package com.job.portal.configurations;

import java.io.IOException;
import java.io.Serializable;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
public class JwtAuthenticationEntryPoint implements Serializable,AuthenticationEntryPoint{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException, ServletException {
		
		response.setContentType("application/json");
		
		
		String jsonResponse;
		
		
		String errorMessage = response.getHeader("Error");
		
		if( errorMessage != null && errorMessage.equals("Jwt Expired")) {
			 
			 jsonResponse = "{\r\n"+ "	\"message\": \"Jwt expired\",\r\n" + "	\"msgKey\": \"JP-E032208\"\r\n" + "}";
			
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			response.getOutputStream().println(jsonResponse);
			
		}else if(errorMessage != null && errorMessage.equals("Malformed Jwt") ) {
			
			 jsonResponse = "{\r\n"+ "	\"message\": \"Malformed Jwt \",\r\n" + "	\"msgKey\": \"JP-E032208\"\r\n" + "}";
			
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getOutputStream().println(jsonResponse);
		}else {
			
			System.out.println(authException.getMessage());
			
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			 jsonResponse = "{\r\n"+" \"message\" : \"Unauthorized user \" , \r\n"  
					+ " \"msgKey\": \"Unauthorized\"\r\n "+ "}";
		
			 System.out.println(authException.getLocalizedMessage());
			 System.out.println(authException.getCause());
		response.getOutputStream().println(jsonResponse);
		}
		
	}

}
