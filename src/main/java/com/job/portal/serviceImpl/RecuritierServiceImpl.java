package com.job.portal.serviceImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.job.portal.dto.ApplicantDto;
import com.job.portal.dto.GetAllJobAndAllApplicant;
import com.job.portal.dto.JobAndAllApplicantDto;
import com.job.portal.dtos.ListResponseDto;
import com.job.portal.dtos.PaginationResponse;
import com.job.portal.entities.JobMaster;
import com.job.portal.idtos.IJobListDto;
import com.job.portal.idtos.JobApplicantDto;
import com.job.portal.repositories.JobMasterRepository;
import com.job.portal.repositories.UserJobRepository;
import com.job.portal.service.RecuritierService;
import com.job.portal.utils.GlobalFunctions;

@Service
public class RecuritierServiceImpl implements RecuritierService {

	@Autowired
	private UserJobRepository userJobRepository;
	
	
	
	@Autowired
	private JobMasterRepository jobRepository;

	@Override
	public ListResponseDto getAllApplicantOfAllJob(String pageNo, String pageSize, Integer userLoggedId) {

		Pageable pageable = GlobalFunctions.getPagination(pageNo, pageSize);
		Page<IJobListDto> fetchAl = userJobRepository.getAllApplicantOfAllJob(pageable, userLoggedId);

		List<IJobListDto> fetchAll = fetchAl.getContent();
		PaginationResponse pagination = new PaginationResponse();
		pagination.setPageNo(fetchAl.getNumber() + 1);
		pagination.setPageSize(fetchAl.getSize());
		pagination.setTotal(fetchAl.getTotalElements());

		List<GetAllJobAndAllApplicant> joblist = new ArrayList<>();

		for (IJobListDto job : fetchAll) {

			GetAllJobAndAllApplicant jobs = new GetAllJobAndAllApplicant();
			jobs.setTitle(job.getTitle());
			jobs.setDesc(job.getDescription());

			jobs.setPostedDate(job.getPostedDate());

			List<String> applicantsDataList = new ArrayList<>();

			applicantsDataList.add(job.getApplicants());

			List<ApplicantDto> applicantDtolist = new ArrayList();
			ObjectMapper object = new ObjectMapper();

			for (String applicantData : applicantsDataList) {
				// Deserialize each JSON string into ApplicantDto
				ApplicantDto[] applicants;
				try {
					applicants = object.readValue(applicantData, ApplicantDto[].class);
					applicantDtolist.addAll(Arrays.asList(applicants));
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			jobs.setApplicants(applicantDtolist);
			joblist.add(jobs);
		}

		return new ListResponseDto("data fetch successfully", joblist, pagination);
	}

	@Override
	public JobAndAllApplicantDto getJobByIdAndApplicants(Integer jobId) {
		
		Optional<JobMaster> jobMaster = jobRepository.findByIdAndIsActiveTrue(jobId);
		
		if(jobMaster.isEmpty()) {
			
			return null;
		} 
		
		 List<JobApplicantDto>  allApplicant =  userJobRepository.getJobAllAplicant(jobId);
		 
		 JobAndAllApplicantDto getJobAndAllApplicant = new JobAndAllApplicantDto();
		 
		 getJobAndAllApplicant.setTitle(jobMaster.get().getTitle());
		 getJobAndAllApplicant.setDesc(jobMaster.get().getDescription());
		 getJobAndAllApplicant.setPostedDate(jobMaster.get().getPostedDate());
		 getJobAndAllApplicant.setApplicants(allApplicant);
		 
		
		return getJobAndAllApplicant;
	}
	
	
	
	

}
