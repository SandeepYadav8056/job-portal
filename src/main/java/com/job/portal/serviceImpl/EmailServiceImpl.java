package com.job.portal.serviceImpl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.job.portal.service.EmailService;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import jakarta.validation.Valid;

@Service
public class EmailServiceImpl implements EmailService {

	
	@Autowired
	private JavaMailSender javaMailSender;
	
	@Value("${spring.mail.username}")
	private String sendFrom;

	@Override
	public void SendSimpleMessage(String sendTo, String subject, String text) throws MessagingException {
		
		
		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
		
		helper.setFrom(sendFrom);
		helper.setTo(sendTo);
		helper.setSentDate(new Date());
		helper.setText(text, true);
		helper.setSubject(subject);
		javaMailSender.send(mimeMessage);
		
		
	}
	
	
	
	
}
