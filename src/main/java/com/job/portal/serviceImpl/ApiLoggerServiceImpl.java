package com.job.portal.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.job.portal.entities.ApiLogger;
import com.job.portal.repositories.ApiLoggerRepository;
import com.job.portal.service.ApiLoggerService;

@Service
public class ApiLoggerServiceImpl  implements ApiLoggerService{

	@Autowired
	private ApiLoggerRepository apiLoggerRepository;
	
	@Override
	public void createApiLogger(ApiLogger apiDetails) {
		apiLoggerRepository.save(apiDetails);
	}

	
	
}
