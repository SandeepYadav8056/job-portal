package com.job.portal.serviceImpl;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.job.portal.entities.OtpEntity;
import com.job.portal.entities.UserMaster;
import com.job.portal.repositories.OtpRepository;
import com.job.portal.service.ForgotPasswordService;

@Service
public class ForgetPasswordServiceImpl implements ForgotPasswordService {
	
	@Autowired
	private OtpRepository otpRepository;

	@Override
	public void saveOtp(int otp, UserMaster user, LocalDateTime expiry) {
		
		otpRepository.deleteAllByUserId(user.getId());
		OtpEntity newForgotPassword = new OtpEntity();
		newForgotPassword.setExpireAt(expiry);
		newForgotPassword.setOtp(otp);
		newForgotPassword.setUserMaster(user);
		otpRepository.save(newForgotPassword);
		
		
	}

	
}
