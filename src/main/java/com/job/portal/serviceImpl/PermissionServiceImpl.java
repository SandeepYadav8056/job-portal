package com.job.portal.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.job.portal.dto.AddPermissonDto;
import com.job.portal.entities.PermissionMaster;
import com.job.portal.exceptions.ResourceNotFoundException;
import com.job.portal.repositories.PermissionRepository;
import com.job.portal.service.PermissionService;

@Service
public class PermissionServiceImpl  implements PermissionService{

	
	@Autowired
	private PermissionRepository permissionRepository;
	
	@Override
	public Boolean addPermission(AddPermissonDto request,Integer loggedUserId) {
		
		PermissionMaster  permission = new PermissionMaster();
		
		
		permission.setPermissionAction(request.getPermissoinAction());
		permission.setPermissionDesc(request.getPermissonDesc());
		permission.setUrl(request.getUrl());
		permission.setMethod(request.getMethod());
		permission.setCreatedBy(loggedUserId);
		permission.setUpdatedBy(loggedUserId);
		
		PermissionMaster permissionadded = permissionRepository.save(permission);
		if(permissionadded != null)
			return true ;
		else
			return false;
	}
	
	
	public String deletePermission(Integer id ,Integer userLoggedId ) {
		
		PermissionMaster existingPermission = permissionRepository.findByIdAndIsActiveTrue(id);
		
		if(existingPermission != null) {
			throw new ResourceNotFoundException("Permission not found");
		}
		permissionRepository.deletePermissionById(id,userLoggedId);
		return "Permission  added successfully";
	}
	
	
	public String updatePermission(Integer id ,Integer userLoggedId,AddPermissonDto request) {
		
		PermissionMaster existingPermission = permissionRepository.findByIdAndIsActiveTrue(id);
		
		if(null == existingPermission ) {
			throw new ResourceNotFoundException("Permission not found");
		}
		
		
		existingPermission.setPermissionAction(request.getPermissoinAction());
		existingPermission.setPermissionDesc(request.getPermissonDesc());
		existingPermission.setUrl(request.getUrl());
		existingPermission.setMethod(request.getMethod());
		existingPermission.setUpdatedBy(userLoggedId);
		permissionRepository.save(existingPermission);
		return "Permission updated successfully";
	}
	
	

}
