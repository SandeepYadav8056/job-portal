package com.job.portal.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.job.portal.configurations.RedisConfig;

@Service
public class CacheOperation {

	@Autowired
	private RedisConfig redisConfig;

	public CacheOperation() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	 	public boolean isKeyExist(String key1 ,String key2) {
	 		return redisConfig.redisTemplate().opsForHash().hasKey(key1, key2);
	 	}
	 	
	 	public void addInCache(String key1 , String key2 ,Object val ) {
	 		redisConfig.redisTemplate().opsForHash().put(key1, key2, val);
	 	}
	 	
	 	public Object getFromCache(String key1 ,String key2) {
	 		return redisConfig.redisTemplate().opsForHash().get(key1, key2);
	 	}

	 	public void removeKeyFromCache(String key) {
	 		redisConfig.redisTemplate().delete(key);
	 	}
	 	
	 	public void removeAllKeyFromCache() {
	 		redisConfig.redisConnectionFactory().getConnection().serverCommands().flushAll();
	 	}
}

