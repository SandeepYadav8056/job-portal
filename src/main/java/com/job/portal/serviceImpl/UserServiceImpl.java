package com.job.portal.serviceImpl;

import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMethod;

import com.job.portal.dto.RegisterRequestDto;
import com.job.portal.entities.RoleMaster;
import com.job.portal.entities.UserMaster;
import com.job.portal.exceptions.ResourceNotFoundException;
import com.job.portal.idtos.ICandidateJobExportDto;
import com.job.portal.idtos.IJobListByUserDto;
import com.job.portal.idtos.IUserListDto;
import com.job.portal.repositories.RoleMasterRepository;
import com.job.portal.repositories.UserMasterRepository;
import com.job.portal.service.UserService;
import com.job.portal.utils.ExportExcelFileUtil;
import com.job.portal.utils.GlobalFunctions;

import jakarta.servlet.http.HttpServletResponse;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private RoleMasterRepository roleRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserMasterRepository userRepository;
	
	@Autowired
	private  CacheOperation cache;

	@Override
	public RegisterRequestDto registerRecuritier(RegisterRequestDto requestDto,
			@RequestAttribute(GlobalFunctions.CUSTOM_USER_ID) Integer loggedId) {

		Optional<RoleMaster> roleMaster = roleRepository.findByRoleNameAndIsActiveTrue("Recuriter");

		UserMaster recuriter = new UserMaster();
		recuriter.setFullName(requestDto.getFullName());
		recuriter.setEmail(requestDto.getEmail());
		recuriter.setGender(requestDto.getGender());
		recuriter.setPassword(passwordEncoder.encode(requestDto.getPassword()));
		recuriter.setRoleMaster(roleMaster.get());

		UserMaster save = userRepository.save(recuriter);

		return new RegisterRequestDto(save.getFullName(), save.getEmail(), save.getGender(), save.getPassword());
	}

	@Override
	public Page<IUserListDto> getAllUser(String pageNo, String pageSize) {

		Pageable pageable = GlobalFunctions.getPagination(pageNo, pageSize);
		Page<IUserListDto> userslist = userRepository.getAllCandidate(pageable);
		return userslist;
	}

	@Override
	public Page<IUserListDto> getAllRecuriter(String pageNo, String pageSize) {
		Pageable pageable = GlobalFunctions.getPagination(pageNo, pageSize);

		Page<IUserListDto> recuriterlist = userRepository.getAllRecuriter(pageable);

		return recuriterlist;
	}

	@Override
	public Page<IJobListByUserDto> getAppliedJobByApplicant(Integer userId, String pageNo, String pageSize) {

		Pageable pageable = GlobalFunctions.getPagination(pageNo, pageSize);
		Page<IJobListByUserDto> fetchAll = userRepository.getAppliedJobByCandidate(userId, pageable);
		return fetchAll;
	}

	@Override
	public void deleteUserById(Integer id) {

		UserMaster user = userRepository.findByIdAndIsActiveTrue(id);

		if (user == null) {
			throw new ResourceNotFoundException("User not found.");
		}
		userRepository.deleteById(id);
		if(cache.isKeyExist(user.getEmail(), user.getEmail())) {
			cache.removeKeyFromCache(user.getEmail());
		}
	}

	@Override
	public String updateUser(Integer id, RegisterRequestDto request) {

		UserMaster userMaster = userRepository.findByIdAndIsActiveTrue(id);

		if (userMaster == null) {
			throw new ResourceNotFoundException("User not found.");
		}

		if (userMaster.getRoleMaster().getRoleName().equalsIgnoreCase("Recuriter")) {

			UserMaster master = UserDtoToUserMaster(userMaster, request);
			userRepository.save(master);
			
				cache.removeAllKeyFromCache();
			return "Recuritier updated  successfully";

		} else {
			UserMaster master = UserDtoToUserMaster(userMaster, request);
			userRepository.save(master);
			cache.removeAllKeyFromCache();
			return "Candidate updated  successfully";
		}
	}

	public UserMaster UserDtoToUserMaster(UserMaster userMaster, RegisterRequestDto request) {

		userMaster.setEmail(request.getEmail());
		userMaster.setFullName(request.getFullName());
		userMaster.setGender(request.getGender());
		userMaster.setPassword(passwordEncoder.encode(request.getPassword()));
		return userMaster;
	}

	@Override
	public ByteArrayInputStream exportCandidateExcel() throws Exception {
		
		Pageable pageable = Pageable.unpaged();
		List<IUserListDto> allCandidate = userRepository.getAllCandidate(pageable).getContent();
		ByteArrayInputStream exportUserToExcel = ExportExcelFileUtil.exportUserToExcel(allCandidate,"CandidateDetails");
		return exportUserToExcel;
	}
	
	@Override
	public ByteArrayInputStream exportRecuriterExcel() throws Exception {
		
		Pageable pageable = Pageable.unpaged();
		List<IUserListDto> allCandidate = userRepository.getAllCandidate(pageable).getContent();
		ByteArrayInputStream exportUserToExcel = ExportExcelFileUtil.exportUserToExcel(allCandidate,"RecuriterDetails");
		return exportUserToExcel;
	}

	@Override
	public ByteArrayInputStream exportCandidateJobExcel() throws Exception {
	
		List<ICandidateJobExportDto> candidateJob = userRepository.getAllCandidateJob();
		ByteArrayInputStream exportUserJobToExcel = ExportExcelFileUtil.exportCandidateJobToExcel(candidateJob);
		return exportUserJobToExcel;
	}
	
	
	
	
	
	

}
