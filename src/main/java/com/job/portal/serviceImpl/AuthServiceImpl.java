package com.job.portal.serviceImpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.job.portal.dto.ForgotPasswordConfirmDto;
import com.job.portal.dto.RegisterRequestDto;
import com.job.portal.dtos.ErrorResponseDto;
import com.job.portal.entities.OtpEntity;
import com.job.portal.entities.RoleMaster;
import com.job.portal.entities.UserMaster;
import com.job.portal.exceptions.ResourceNotFoundException;
import com.job.portal.repositories.OtpRepository;
import com.job.portal.repositories.PermissionRepository;
import com.job.portal.repositories.RoleMasterRepository;
import com.job.portal.repositories.UserMasterRepository;
import com.job.portal.service.AuthService;
import com.job.portal.service.EmailService;
import com.job.portal.service.ForgotPasswordService;
import com.job.portal.utils.ErrorMessageConstant;
import com.job.portal.utils.MailTemplateUtil;
import com.job.portal.utils.Validator;

import jakarta.mail.MessagingException;

@Service
public class AuthServiceImpl implements AuthService, UserDetailsService {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private RoleMasterRepository roleRepository;

	@Autowired
	private EmailService emailService;

	@Autowired
	private UserMasterRepository userRepository;

	@Autowired
	private ForgotPasswordService forgotPasswordService;
	
	@Autowired
	private PermissionRepository permissionRepository;

	@Autowired
	private OtpRepository otpRepository;
	
	@Autowired
	private CacheOperation cache;

	@Override
	public void registerCandidate(RegisterRequestDto request) {

		RoleMaster role = roleRepository.findByRoleNameAndIsActiveTrue("Candidate")
				.orElseThrow(() -> new ResourceNotFoundException(ErrorMessageConstant.ROLE_NOT_FOUND));

		UserMaster userMaster = new UserMaster();
		userMaster.setFullName(request.getFullName());
		userMaster.setEmail(request.getEmail());
		userMaster.setGender(request.getGender());
		userMaster.setPassword(passwordEncoder.encode(request.getPassword()));
		userMaster.setRoleMaster(role);
		userRepository.save(userMaster);
	}

	@Override
	public Boolean comparePassword(String password, String hashPassword) {
		return passwordEncoder.matches(password, hashPassword);
	}

	@Override
	public ArrayList<String> getPermissionByUser(Integer userId) {
		
		UserMaster user = userRepository.findByIdAndIsActiveTrue(userId);
		
		ArrayList<String> permission = permissionRepository.findPermissionByUser(userId);
		return permission;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		UserMaster user = new UserMaster();
		
		if(!cache.isKeyExist(username, username)) {
			
			user  = userRepository.findByEmailIgnoreCaseAndIsActiveTrue(username)
					.orElseThrow(()->new ResourceNotFoundException("User not found with Email"));
			
			cache.addInCache(username, username, user.toString());
	
		}else {
			
			String jsonString = (String) cache.getFromCache(username, username);
			
			
			try {
				ObjectMapper mapper = new ObjectMapper();
				Map<String, Object> map = mapper.readValue(jsonString,Map.class);
				System.out.println(map.toString());
				user.setEmail( (String) map.get("email") );
				user.setPassword((String) map.get("password"));
				user.setId(((Integer) map.get("id")));;
			}catch (Exception e) {
				System.out.println(e);
			}
			
		}
		
		if(user.getEmail().isEmpty()) {
			throw new ResourceNotFoundException("User not found exception");
		}
		return new User(user.getEmail(), user.getPassword(), getAuthorities(user));
	}
	
	

	private Collection<? extends GrantedAuthority> getAuthorities(UserMaster user) {
		

		ArrayList<GrantedAuthority> authorities = new ArrayList<>();

		if (user.getId() != null) {
			// add permission repository get all permission and set to this
			ArrayList<String> permissions =  getPermissionByUser(user.getId());
			permissions.stream().forEach(
				p -> {
						authorities.add(new SimpleGrantedAuthority("ROLE_"+p));
					});

			//authorities.add(new SimpleGrantedAuthority("ROLE_" + "candidate"));
		}
		return authorities;
	}

	@Override
	public void generateOtpAndSendEmail(String email, UserMaster user) throws MessagingException {

		int otp = generateOtp();

		String otpString = Integer.toString(otp);

		LocalDateTime dateTime = LocalDateTime.now();
		LocalDateTime expiredAt = dateTime.plusMinutes(5);

		forgotPasswordService.saveOtp(otp, user, expiredAt);

		String otpTemplate = MailTemplateUtil.getOtpTemplate().replace("{OTP}", otpString);

		emailService.SendSimpleMessage(user.getEmail(), "Job Portal - Password Reset", otpTemplate);
	}

	private int generateOtp() {

		int min = 100000;
		int max = 999999;
		return (int) Math.floor(Math.random() * (max - min + 1) + min);
	}

	@Override
	public String createForgotPasswordConfirm(ForgotPasswordConfirmDto forgotPasswordConfirmDto) {
		
	
		

		Optional<UserMaster> user = userRepository
				.findByEmailIgnoreCaseAndIsActiveTrue(forgotPasswordConfirmDto.getEmail());
		
	

		if (!user.isPresent()) {
			throw new IllegalArgumentException("Invalid Email");
		}

		Integer otp = Integer.parseInt(forgotPasswordConfirmDto.getOtp());

		OtpEntity otpEntity = otpRepository.findbyOtp(otp);

		if (otpEntity == null || LocalDateTime.now().isAfter(otpEntity.getExpireAt())) {
			throw new ResourceNotFoundException("Otp Invalid or Expired");

		}

		UserMaster userMaster = user.get();
		userMaster.setPassword(passwordEncoder.encode(forgotPasswordConfirmDto.getPassword()));
		userRepository.save(userMaster);
		return "Password has been reset successfully.";

	}

}
