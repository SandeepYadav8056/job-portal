package com.job.portal.serviceImpl;

import java.time.LocalDate;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.job.portal.dto.JobDto;
import com.job.portal.entities.JobMaster;
import com.job.portal.entities.UserMaster;
import com.job.portal.exceptions.ResourceAlreadyExistException;
import com.job.portal.exceptions.ResourceNotFoundException;
import com.job.portal.idtos.IGetAllJob;
import com.job.portal.repositories.JobMasterRepository;
import com.job.portal.repositories.UserMasterRepository;
import com.job.portal.service.JobService;
import com.job.portal.utils.GlobalFunctions;

@Service
public class JobServiceImpl implements JobService{
	
	@Autowired
	private UserMasterRepository userRepository;
	
	@Autowired
	private JobMasterRepository jobRepository;

	@Override
	public JobDto createJob(Integer loginId, JobDto request) {
		
		if(request ==  null ) {
			throw new IllegalArgumentException("request payload is null");
		}
		
		if( jobRepository.findByJobTitleAndRecruiter(request.getTitle(),loginId)) {
			throw new ResourceAlreadyExistException("Job already exists with this title.");
		}
		
		UserMaster user = userRepository.findByIdAndIsActiveTrue(loginId);
		
		JobMaster createJob = new JobMaster();
		createJob.setTitle(request.getTitle());
		createJob.setDescription(request.getDesc());
		createJob.setUserMaster(user);
		createJob.setPostedDate(LocalDate.now());
		JobMaster result = jobRepository.save(createJob);
		 return new JobDto(result.getTitle(),result.getDescription());
	}

	@Override
	public String updateJob(Integer userLoggedId, JobDto request, Integer jobId) {
		
		JobMaster job = jobRepository.findByIdAndIsActiveTrue(jobId).orElseThrow(()-> new ResourceNotFoundException("Job not found."));
		job.setTitle(request.getTitle());
		job.setDescription(request.getDesc());
		job.setUpdatedBy(userLoggedId);
		
		jobRepository.save(job);
		return "job updated successfully";
	}

	@Override
	public String deleteJobById(Integer jobId, Integer userLoggedId) {
		
		Optional<JobMaster> existingJob = jobRepository.findByIdAndIsActiveTrue(jobId);
		if(!existingJob.isEmpty()) {
			jobRepository.deleteJob(existingJob.get().getId(),userLoggedId);
			return "Job deleted successfully.";
		}else {
			throw new ResourceNotFoundException("Job not found.");
		}
	}

	@Override
	public Page<IGetAllJob> getAllJobs(String pageNo, String pageSize) {
		
		Pageable pageable = GlobalFunctions.getPagination(pageNo, pageSize);
		
		Page<IGetAllJob> fetchAll = jobRepository.fetAllJob(pageable);
		
		return fetchAll;
	}

	@Override
	public Page<IGetAllJob> fetchJobByRecuritier(Integer userLoggedIn, String pageNo, String pageSize) {
		Pageable pageable = GlobalFunctions.getPagination(pageNo, pageSize);
		Page<IGetAllJob> fetch =  jobRepository.fetchJobByRecuritier(userLoggedIn,pageable);
		return fetch;
	}

	
	
	

}
