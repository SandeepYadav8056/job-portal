package com.job.portal.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.job.portal.dto.AssignPermisionToRoleDto;
import com.job.portal.dto.RoleRequestDto;
import com.job.portal.entities.PermissionMaster;
import com.job.portal.entities.RoleMaster;
import com.job.portal.entities.RolePermission;
import com.job.portal.entities.RolePermissionId;
import com.job.portal.exceptions.ResourceNotFoundException;
import com.job.portal.repositories.PermissionRepository;
import com.job.portal.repositories.RoleMasterRepository;
import com.job.portal.repositories.RolePermissionRepository;
import com.job.portal.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService {

	
	@Autowired
	private RoleMasterRepository roleRepository;
	
	@Autowired
	private PermissionRepository permissionRepository;
	
	@Autowired
	private RolePermissionRepository rolePermissionRepository;
	
	@Override
	public RoleRequestDto addRole(RoleRequestDto request) {
		
		RoleMaster roleMaster = new RoleMaster();
		roleMaster.setRoleName(request.getRoleName());
		roleMaster.setRoleDesc(request.getRoleDesc());		
		RoleMaster result = roleRepository.save(roleMaster);
		return new RoleRequestDto(result.getRoleName(),result.getRoleDesc());
	}

	@Override
	public String assignPermissionToRole(AssignPermisionToRoleDto request, Integer userLoggedId) {
	
		
		List<PermissionMaster> permissions = permissionRepository.findAllByIdAndIsActiveTrue(request.getPermissionId());
		
		RoleMaster role = roleRepository.findByIdAndIsActiveTrue(request.getRoleId());
		
		List<Integer> foundPermissionId = permissions.stream().map(PermissionMaster::getId).collect(Collectors.toList());
		
		
		List<Integer> notFoundIds  = request.getPermissionId().stream().filter( pid -> !foundPermissionId.contains(pid)).collect(Collectors.toList());
		
		ArrayList<RolePermission> rolePermissionList = new ArrayList<>();
		
		if(!notFoundIds.isEmpty()) {
			throw new ResourceNotFoundException("permission not found ids : "+ notFoundIds);
		}
	
		
		for(PermissionMaster permission : permissions) {
			
		
		RolePermissionId rolePermissionId = new RolePermissionId(role.getId(), permission.getId());
	
		RolePermission rolePermission = new  RolePermission();
		rolePermission.setRolePermissionId(rolePermissionId);
		rolePermission.setPermissionMaster(permission);
		rolePermission.setRoleMaster(role);
		
		rolePermissionList.add(rolePermission);
		}
		
		rolePermissionRepository.saveAll(rolePermissionList);
		return "Permission assigned successfully";
	}

	@Override
	public void deletePermissionFromRole() {
		// TODO Auto-generated method stub
		
	}

//	@Override
//	public void deletePermissionFromRole(AssignPermisionToRoleDto request) {
//		
//
//		
//		
//		
//	}

}
