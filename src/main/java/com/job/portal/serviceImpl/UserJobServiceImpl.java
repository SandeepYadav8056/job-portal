package com.job.portal.serviceImpl;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.job.portal.entities.JobMaster;
import com.job.portal.entities.UserJobMaster;
import com.job.portal.entities.UserMaster;
import com.job.portal.enums.Status;
import com.job.portal.exceptions.ResourceAlreadyExistException;
import com.job.portal.repositories.JobMasterRepository;
import com.job.portal.repositories.UserJobRepository;
import com.job.portal.repositories.UserMasterRepository;
import com.job.portal.service.UserJobService;
import com.job.portal.utils.MailTemplateUtil;

import jakarta.mail.MessagingException;

@Service
public class UserJobServiceImpl implements UserJobService{
	
	@Autowired
	private UserJobRepository userJobRepository;
	
	@Autowired
	private JobMasterRepository jobRepository;
	
	@Autowired
	private UserMasterRepository userRepository;
	
	
	@Autowired
	private EmailServiceImpl mailService;
	

	@Override
	public String applyforJob(Integer jobId, Integer userLoggedId) {
		Optional<JobMaster> job = jobRepository.findByIdAndIsActiveTrue(jobId);
		UserMaster user = userRepository.findByIdAndIsActiveTrue(userLoggedId);
		if(jobRepository.alreadyAppliedforJob(jobId,userLoggedId)) {
			throw new ResourceAlreadyExistException("User already applied for this job");
		}
		

		
		UserMaster recturiter = null ;
		if(job.isPresent() && job.get().getUserMaster().getIsActive().equals(true)) {
		
		  recturiter =job.get().getUserMaster();
		
		}
		
			UserJobMaster userJob = new UserJobMaster();
			userJob.setApplicationDate(LocalDate.now());
			userJob.setStatus(Status.SUBMITTED);
		
			userJob.setUserMaster(user);
			

			userJob.setJobMaster(job.get());
			userJob.setCreatedBy(userLoggedId);
			userJob.setUpdatedBy(userLoggedId);
	
			userJobRepository.save(userJob);
			
			String candidateTemplate = MailTemplateUtil.getCandidateMailTemplate().replace("[Candidate_Name]", user.getFullName()).replace("[Job_Position_Title]", job.get().getTitle()).replace("[Recuritier_NAME]",recturiter.getFullName());			
			String recuritierTemplate = MailTemplateUtil.getRecuritierMailTemplate().replace("[Job Position Title]",job.get().getTitle()).replace("[Candidate_Name]", user.getFullName()).replace("[Candidate_Email]", user.getEmail()).replace("[Recruiter_Name]",recturiter.getFullName()); 
			
		
			
				try {
					mailService.SendSimpleMessage( user.getEmail(), "Job Candidate Applied Notification", candidateTemplate);
					mailService.SendSimpleMessage(recturiter.getEmail(), "Job Recuriter Applied Notification", recuritierTemplate);
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			return "Applied successfully";
		
	}
	
	

}
