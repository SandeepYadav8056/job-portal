package com.job.portal.controller;

import java.io.ByteArrayInputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.job.portal.dto.RegisterRequestDto;
import com.job.portal.dtos.ErrorResponseDto;
import com.job.portal.dtos.ListResponseDto;
import com.job.portal.dtos.PaginationResponse;
import com.job.portal.dtos.SuccessResponseDto;
import com.job.portal.idtos.IJobListByUserDto;
import com.job.portal.idtos.IUserListDto;
import com.job.portal.service.UserService;
import com.job.portal.utils.Constant;
import com.job.portal.utils.ErrorMessageKey;
import com.job.portal.utils.GlobalFunctions;
import com.job.portal.utils.SuccessMessageConstant;

@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@PreAuthorize("hasAuthority('ROLE_RecuritierAdd')")
	@PostMapping("/recuriters/register")
	public ResponseEntity<?> addRecuritier(@RequestBody RegisterRequestDto request,
			@RequestAttribute(GlobalFunctions.CUSTOM_USER_ID) Integer userLoggedId) {

		try {
			RegisterRequestDto registerRecuritier = userService.registerRecuritier(request, userLoggedId);

			return new ResponseEntity<>(new SuccessResponseDto("Recuriter registered successfully", "201"),
					HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "400"), HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasAuthority('ROLE_CandidateList')")
	@GetMapping("/candidates")
	public ResponseEntity<?> getAllCandidate(@RequestParam(name = Constant.PAGENUMBER, defaultValue = "") String pageNo,
			@RequestParam(name = Constant.PAGESIZE, defaultValue = "") String pageSize) {
		try {

			Page<IUserListDto> users = userService.getAllUser(pageNo, pageSize);

			PaginationResponse pagination = new PaginationResponse();
			pagination.setPageNo(users.getNumber() + 1);
			pagination.setPageSize(users.getSize());
			pagination.setTotal(users.getTotalElements());

			return new ResponseEntity<>(
					new ListResponseDto(SuccessMessageConstant.DATA_FETCH_SUCCESSFULLY, users.getContent(), pagination),
					HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), ErrorMessageKey.USER_E031102),
					HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasAuthority('ROLE_RecruiterList')")
	@GetMapping("/recruiters")
	public ResponseEntity<?> getAllRecuriter(@RequestParam(name = Constant.PAGENUMBER, defaultValue = "") String pageNo,
			@RequestParam(name = Constant.PAGESIZE, defaultValue = "") String pageSize) {
		try {
			Page<IUserListDto> users = userService.getAllRecuriter(pageNo, pageSize);

			PaginationResponse pagination = new PaginationResponse();
			pagination.setPageNo(users.getNumber() + 1);
			pagination.setPageSize(users.getSize());
			pagination.setTotal(users.getTotalElements());

			return new ResponseEntity<>(
					new ListResponseDto(SuccessMessageConstant.DATA_FETCH_SUCCESSFULLY, users.getContent(), pagination),
					HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), ErrorMessageKey.USER_E031102),
					HttpStatus.BAD_REQUEST);
		}

	}

	@PreAuthorize("hasAuthority('ROLE_UserEdit')")
	@PutMapping("/recuriter/{id}")
	public ResponseEntity<?> updateRecuriter(@PathVariable("id") Integer id, @RequestBody RegisterRequestDto request) {

		try {

			String result = userService.updateUser(id, request);
			return new ResponseEntity<>(new SuccessResponseDto(SuccessMessageConstant.USER_UPDATED_SUCCESSFULLY, "200"), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), ErrorMessageKey.USER_E031102),
					HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasAuthority('ROLE_ApplicantJobList')")
	@GetMapping("/applicants/{id}/applied-jobs")
	public ResponseEntity<?> getAppliedJobByApplicant(@PathVariable(name = "id") Integer id,
			@RequestParam(name = Constant.PAGENUMBER, defaultValue = "") String pageNo,
			@RequestParam(name = Constant.PAGESIZE, defaultValue = "") String pageSize) {

		try {
			Page<IJobListByUserDto> applicants = userService.getAppliedJobByApplicant(id, pageNo, pageSize);

			PaginationResponse pagination = new PaginationResponse();
			pagination.setPageNo(applicants.getNumber() + 1);
			pagination.setPageSize(applicants.getSize());
			pagination.setTotal(applicants.getTotalElements());

			return new ResponseEntity<>(new ListResponseDto(SuccessMessageConstant.DATA_FETCH_SUCCESSFULLY,
					applicants.getContent(), pagination), HttpStatus.OK);

		} catch (Exception e) {
			System.out.println(e.getStackTrace());
			System.out.println(e.getLocalizedMessage());
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), ErrorMessageKey.USER_E031102),
					HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasAuthority('ROLE_UserDelete')")
	@DeleteMapping("users/{id}")
	public ResponseEntity<?> deleteUser(@PathVariable(name = "id") Integer id) {
		try {

			userService.deleteUserById(id);
			return new ResponseEntity<>(new SuccessResponseDto("User deleted successfully", "PJ-031212"),
					HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "400"), HttpStatus.BAD_REQUEST);
		}
	}

	@PreAuthorize("hasAuthority('ROLE_CandidateListExport')")
	@GetMapping("/export-candidates")
	public ResponseEntity<?> exportCandidate() {

		try {

			String fileName = "candidate.xlsx";

			ByteArrayInputStream exportCandidateData = userService.exportCandidateExcel();

			Resource resource = new InputStreamResource(exportCandidateData);

			return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName)
					.contentType(MediaType.parseMediaType("application/vnd.ms-excel")).body(resource);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(),"400"),HttpStatus.BAD_REQUEST);
		}
	}
	
	@PreAuthorize("hasAuthority('ROLE_RecuriterListExport')")
	@GetMapping("/export-recuriter")
	public ResponseEntity<?> exportRecuriter() {

		try {

			String fileName = "recuriter.xlsx";

			ByteArrayInputStream exportCandidateData = userService.exportCandidateExcel();

			Resource resource = new InputStreamResource(exportCandidateData);

			return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName)
					.contentType(MediaType.parseMediaType("application/vnd.ms-excel")).body(resource);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(),"400"),HttpStatus.BAD_REQUEST);
		}
	}
	
	@PreAuthorize("hasAuthority('ROLE_CandidateJobExport')")
	@GetMapping("/export-candidate-job")
	public ResponseEntity<?> exportCandidateJobExcel() {

		try {

			String fileName = "candidateJob.xlsx";

			ByteArrayInputStream exportCandidateData = userService.exportCandidateJobExcel();

			Resource resource = new InputStreamResource(exportCandidateData);

			return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName)
					.contentType(MediaType.parseMediaType("application/vnd.ms-excel")).body(resource);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(),"400"),HttpStatus.BAD_REQUEST);
		}
	}
	
}
