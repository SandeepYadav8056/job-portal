package com.job.portal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.job.portal.dtos.ErrorResponseDto;
import com.job.portal.dtos.SuccessResponseDto;
import com.job.portal.service.UserJobService;
import com.job.portal.utils.GlobalFunctions;

@RestController
@RequestMapping("/job-application")
public class UserJobController {

	@Autowired
	private UserJobService userJobService;

	@PreAuthorize("hasAuthority('ROLE_JobApply')")
	@PostMapping("/apply")
	public ResponseEntity<?> applyForJob(@RequestParam(name = "jobId",required = true) Integer jobId,
			@RequestAttribute( GlobalFunctions.CUSTOM_USER_ID) Integer userLoggedId
			) {

		try {
			
			String result = userJobService.applyforJob(jobId, userLoggedId);

			return new ResponseEntity<>(new SuccessResponseDto(result, "200"), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "400"), HttpStatus.BAD_REQUEST);
		}
	}

}
