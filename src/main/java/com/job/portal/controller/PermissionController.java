package com.job.portal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.job.portal.dto.AddPermissonDto;
import com.job.portal.dtos.ErrorResponseDto;
import com.job.portal.dtos.SuccessResponseDto;
import com.job.portal.service.PermissionService;
import com.job.portal.utils.GlobalFunctions;


@RestController
@RequestMapping("/permissions")
public class PermissionController {

	@Autowired
	private PermissionService permissionService;
	
	
	@PostMapping()
	public ResponseEntity<?> addPermission(@RequestBody AddPermissonDto request,@RequestAttribute(GlobalFunctions.CUSTOM_USER_ID) Integer userLoggedId){
		
		try {
			
			permissionService.addPermission(request, userLoggedId);
		
				return new ResponseEntity<>(new SuccessResponseDto("Permission added successfully", "200", request),HttpStatus.OK);
			
		}catch (Exception e) {
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(),"400"),HttpStatus.BAD_REQUEST);
		}
	}
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deletePermission(@PathVariable(name = "id") Integer id ,@RequestAttribute(GlobalFunctions.CUSTOM_USER_ID) Integer userLoggedId ){
		try {
			String deletePermission = permissionService.deletePermission(id, userLoggedId);
			return new ResponseEntity<>(new SuccessResponseDto(deletePermission,"200"),HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "400" ),HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> updatePermission(@PathVariable(name = "id") Integer id , @RequestAttribute(GlobalFunctions.CUSTOM_USER_ID) Integer userLoggedId,@RequestBody AddPermissonDto request ){
		try {
			
			String message = permissionService.updatePermission(id, userLoggedId,request);
			return new ResponseEntity<>(new SuccessResponseDto(message, "200"),HttpStatus.OK );
		}catch (Exception e) {
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(),"400"),HttpStatus.BAD_REQUEST);
		}
	}
	
	
	
}

