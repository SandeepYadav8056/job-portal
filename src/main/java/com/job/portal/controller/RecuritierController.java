package com.job.portal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.job.portal.dto.GetAllJobAndAllApplicant;
import com.job.portal.dto.JobAndAllApplicantDto;
import com.job.portal.dtos.ListResponseDto;
import com.job.portal.dtos.SuccessResponseDto;
import com.job.portal.service.RecuritierService;
import com.job.portal.utils.Constant;
import com.job.portal.utils.GlobalFunctions;

@RestController
@RequestMapping("/recuritiers")
public class RecuritierController {
	
	
	@Autowired
	private RecuritierService recuritierService;
	
	//login recurtire  can see all his job added and all user applied for that job
	@PreAuthorize("hasAuthority('ROLE_JobAndApplicantList')")
	@GetMapping("/jobs")
	public ResponseEntity<?> getAllApplicantForAllJob(@RequestParam(name=Constant.PAGENUMBER,defaultValue = "") String pageNo,@RequestParam(name=Constant.PAGESIZE,defaultValue = "") String pageSize
			,@RequestAttribute(GlobalFunctions.CUSTOM_USER_ID) Integer userLoggedId
			){
		
		ListResponseDto fetchAllJob = recuritierService.getAllApplicantOfAllJob(pageNo,pageSize,userLoggedId);
		
//		PaginationResponse pagination = new PaginationResponse();
//		pagination.setPageNo(fetchAllJob.getNumber()+1);
//		pagination.setPageSize(fetchAllJob.getSize());
//		pagination.setTotal(fetchAllJob.getTotalElements());
		
		return new ResponseEntity<>(fetchAllJob,HttpStatus.OK);
	}

	
	@PreAuthorize("hasAuthority('ROLE_JobAndApplicantView')")
	@GetMapping("/jobs/{id}")
	public ResponseEntity<?> getJobByIdAndApplicants(@PathVariable(name = "id" , required = true) Integer jobId ){
		
		 JobAndAllApplicantDto jobByIdAndApplicants = recuritierService.getJobByIdAndApplicants(jobId);
		
		return new ResponseEntity<>(new SuccessResponseDto("data fetch successfully", "200", jobByIdAndApplicants),HttpStatus.OK);
	
	}
	

}
