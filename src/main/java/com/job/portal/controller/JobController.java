 package com.job.portal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.job.portal.dto.JobDto;
import com.job.portal.dtos.ErrorResponseDto;
import com.job.portal.dtos.ListResponseDto;
import com.job.portal.dtos.PaginationResponse;
import com.job.portal.dtos.SuccessResponseDto;
import com.job.portal.exceptions.ResourceNotFoundException;
import com.job.portal.idtos.IGetAllJob;
import com.job.portal.service.JobService;
import com.job.portal.utils.Constant;
import com.job.portal.utils.GlobalFunctions;

@RestController
@RequestMapping("/jobs")
public class JobController {

	@Autowired
	private JobService jobService;

	@PreAuthorize("hasAuthority('ROLE_JobAdd')")
	@PostMapping()
	public ResponseEntity<?> createJob(@RequestAttribute(GlobalFunctions.CUSTOM_USER_ID) Integer loginId,
			@RequestBody JobDto request) {
		try {
			JobDto result = jobService.createJob(loginId, request);
			return new ResponseEntity<>(new SuccessResponseDto("Job created successfully", "200", request),
					HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "400"), HttpStatus.BAD_REQUEST);
		}
	}
/// add to recurtier permission
	@PreAuthorize("hasAuthority('ROLE_JobEdit')")
	@PutMapping("/{id}")
	public ResponseEntity<?> updateJob(@RequestAttribute(GlobalFunctions.CUSTOM_USER_ID) Integer userLoggedId,
			@RequestBody JobDto request, @PathVariable(name = "id", required = true) Integer jobId) {
		try {

			jobService.updateJob(userLoggedId, request, jobId);
			return new ResponseEntity<>(new SuccessResponseDto("Job updated successfully", "200"), HttpStatus.OK);

		} catch (Exception e) {

			if (e.getCause() instanceof ResourceNotFoundException) {
				return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "404"), HttpStatus.NOT_FOUND);
			} else {
				return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "400"), HttpStatus.BAD_REQUEST);
			}

		}

	}

	@PreAuthorize("hasAuthority('ROLE_JobDelete')")
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteJob(@RequestAttribute(GlobalFunctions.CUSTOM_USER_ID) Integer userLoggedId,
			@PathVariable(name = "id") Integer jobId) {

		try {

			String result = jobService.deleteJobById(jobId, userLoggedId);
			return new ResponseEntity<>(new SuccessResponseDto(result, "200"), HttpStatus.OK);
		} catch (Exception e) {

			if (e.getCause() instanceof ResourceNotFoundException) {

				return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "404"), HttpStatus.NOT_FOUND);
			} else {

				return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "400"), HttpStatus.BAD_REQUEST);
			}
		}
	}

	@PreAuthorize("hasAuthority('ROLE_JobList')")
	@GetMapping()
	public ResponseEntity<?> getAllJobs(@RequestParam(name = "pageNo", defaultValue = "") String pageNo,
			@RequestParam(name = "pageSize", defaultValue = "") String pageSize) {

		Page<IGetAllJob> fetchAll = jobService.getAllJobs(pageNo, pageSize);

		PaginationResponse pagination = new PaginationResponse();
		pagination.setPageNo(fetchAll.getNumber() + 1);
		pagination.setPageSize(fetchAll.getSize());
		pagination.setTotal(fetchAll.getTotalElements());

		return new ResponseEntity<>(new ListResponseDto("fetch data successfully", fetchAll.getContent(), pagination),
				HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('ROLE_JobListByRecuriter')")
	@GetMapping("/recuritier")
	public ResponseEntity<?> getJobAddedByRecuritier(@RequestAttribute(GlobalFunctions.CUSTOM_USER_ID) Integer userLoggedId,@RequestParam(name=Constant.PAGENUMBER,defaultValue = "") String pageNo, @RequestParam(name=Constant.PAGESIZE,defaultValue = "") String pageSize) {

		Page<IGetAllJob> fetchAll = jobService.fetchJobByRecuritier(userLoggedId, pageNo, pageSize);
		
		PaginationResponse pagination = new PaginationResponse();
		pagination.setPageNo(fetchAll.getNumber() + 1);
		pagination.setPageSize(fetchAll.getSize());
		pagination.setTotal(fetchAll.getTotalElements());
		
		return new ResponseEntity<>(new ListResponseDto("fetch data successfully", fetchAll.getContent(), pagination),
				HttpStatus.OK);
	}
 
}
