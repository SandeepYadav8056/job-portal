package com.job.portal.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.HttpClientErrorException.BadRequest;

import com.job.portal.configurations.JwtTokenUtils;
import com.job.portal.dto.EmailDto;
import com.job.portal.dto.ForgotPasswordConfirmDto;
import com.job.portal.dto.JwtRequest;
import com.job.portal.dto.RegisterRequestDto;
import com.job.portal.dtos.AuthResponseDto;
import com.job.portal.dtos.ErrorResponseDto;
import com.job.portal.dtos.SuccessResponseDto;
import com.job.portal.entities.OtpEntity;
import com.job.portal.entities.UserMaster;
import com.job.portal.exceptions.ResourceNotFoundException;
import com.job.portal.repositories.OtpRepository;
import com.job.portal.repositories.UserMasterRepository;
import com.job.portal.service.AuthService;
import com.job.portal.utils.ApiUrls;
import com.job.portal.utils.ErrorMessageConstant;
import com.job.portal.utils.SuccessMessageConstant;
import com.job.portal.utils.SuccessMessageKey;

import jakarta.validation.Valid;

@RestController
@RequestMapping(ApiUrls.AUTH)
public class AuthController {

	@Autowired
	private AuthService authService;

	@Autowired
	private UserMasterRepository userMasterRepository;

	@Autowired
	private JwtTokenUtils jwtTokenUtil;
	
	

	@PostMapping(ApiUrls.REGISTER)
	public ResponseEntity<?> registerCandidate(@Valid @RequestBody RegisterRequestDto request) {

		try {
			authService.registerCandidate(request);

			return new ResponseEntity<>(new SuccessResponseDto("User register successfully", "JP-M032012"),
					HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "400"), HttpStatus.BAD_REQUEST);
		}

	}

	@PostMapping(ApiUrls.LOGIN)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest createAuthenticationToken) {
		try {

			Optional<UserMaster> user = userMasterRepository
					.findByEmailIgnoreCaseAndIsActiveTrue(createAuthenticationToken.getEmail());

			if (!user.isPresent()) {
				throw new ResourceNotFoundException(ErrorMessageConstant.INVALID_INFORMATION);
			}

			UserMaster userMaster = user.get();

			if (this.authService.comparePassword(createAuthenticationToken.getPassword(), userMaster.getPassword())) {

				final UserDetails userDetails = this.authService.loadUserByUsername(userMaster.getEmail());

				final String accessToken = this.jwtTokenUtil.generateToken(userDetails);

				ArrayList<String> permissions = authService.getPermissionByUser(userMaster.getId());

				final String refreshToken = this.jwtTokenUtil.refreshToken(accessToken, userDetails);

				return new ResponseEntity<>(new SuccessResponseDto("success", "200",
						new AuthResponseDto(accessToken, refreshToken, permissions)), HttpStatus.OK);

			} else {
				return new ResponseEntity<>(new ErrorResponseDto(ErrorMessageConstant.INVALID_INFORMATION, "400"),
						HttpStatus.BAD_REQUEST);

			}
		} catch (Exception e) {
				return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "400"), HttpStatus.BAD_REQUEST);
			}
		}
		
		
		@PostMapping(ApiUrls.GENERATE_OTP)
		public ResponseEntity<?> generateOtpAndSendMail(@RequestBody EmailDto emailDto){
			
			try {
			
			UserMaster user = userMasterRepository.findByEmailIgnoreCaseAndIsActiveTrue(emailDto.getEmail()).orElseThrow(()-> new ResourceNotFoundException("Invalid Email"));
			
			authService.generateOtpAndSendEmail(user.getEmail(),user);
			
			return new ResponseEntity<>(new SuccessResponseDto(SuccessMessageConstant.OTP_SENT, SuccessMessageKey.USER_M031101,user.getEmail()),HttpStatus.OK); 
			
			
		}catch (Exception e) {
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(),ErrorMessageConstant.FAILED_TO_SEND_OTP),HttpStatus.BAD_REQUEST);
		}
		
	}
	
	
	@PostMapping(ApiUrls.FORGOT_PASSWORD_CONFIRM)
	public ResponseEntity<?> createForgotPasswordConfirm(@Valid @RequestBody ForgotPasswordConfirmDto forgotPasswordConfirmDto){
		
		
		try {
			
			String message = authService.createForgotPasswordConfirm(forgotPasswordConfirmDto);
			
			return new  ResponseEntity<>(new SuccessResponseDto(message, "200"),HttpStatus.OK);
		}catch (Exception e) {
			
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "400"),HttpStatus.BAD_REQUEST);
		}

	}
	
	
	

}
