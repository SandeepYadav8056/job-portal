 package com.job.portal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.job.portal.dto.AssignPermisionToRoleDto;
import com.job.portal.dto.RoleRequestDto;
import com.job.portal.dtos.ErrorResponseDto;
import com.job.portal.dtos.SuccessResponseDto;
import com.job.portal.service.RoleService;
import com.job.portal.utils.ApiUrls;
import com.job.portal.utils.ErrorMessageKey;
import com.job.portal.utils.GlobalFunctions;
import com.job.portal.utils.SuccessMessageConstant;
import com.job.portal.utils.SuccessMessageKey;

import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;

@RestController
@RequestMapping(ApiUrls.ROLE)
public class RoleController {

	
	@Autowired
	private RoleService roleService;
	
	
	@PostMapping()
	public ResponseEntity<?> addRole(@Valid @RequestBody RoleRequestDto request,HttpServletResponse response){
		
		try {
			
			RoleRequestDto result = roleService.addRole(request);
			
			return new	ResponseEntity<>(
					new SuccessResponseDto(SuccessMessageConstant.ROLE_ADDED, SuccessMessageKey.ROLE_M031201,result), HttpStatus.OK);
		}catch (Exception e) {
			return new	ResponseEntity<>(
					new ErrorResponseDto(e.getMessage(), ErrorMessageKey.ROLE_M031201),HttpStatus.BAD_REQUEST);
		}
		
	}
	
	@PostMapping("/assign-permissions")
	public ResponseEntity<?> AssignPermissionsToRole(@RequestBody AssignPermisionToRoleDto request , @RequestAttribute(GlobalFunctions.CUSTOM_USER_ID)Integer userLoggedId){
		try {
			
			String message = roleService.assignPermissionToRole(request,userLoggedId);
			
			return new ResponseEntity<>(new SuccessResponseDto(message, "200"),HttpStatus.OK);
		}catch (Exception e) {
	
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "400"),HttpStatus.BAD_REQUEST);
		}
		
		
	}
	
	
//	public ResponseEntity<?> deleteRolePermission(@RequestBody AssignPermisionToRoleDto request){
//		
//		roleService.deletePermissionFromRole(request);
//		
//		return null;
//	}
	
	
	
}
