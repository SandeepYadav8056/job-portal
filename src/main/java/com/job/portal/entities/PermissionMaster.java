package com.job.portal.entities;

import java.util.Date;
import java.util.List;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name="permissionMaster")
public class PermissionMaster {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name="permission_action")
	private String permissionAction;
	
	@Column(name="permission_desc")
	private String permissionDesc;
	
	@Column(name ="method")
	private String method;
	
	@Column(name = "url")
	private String url;
	
	
	@Column(name="is_active")
	private Boolean isActive = true;
	
	@CreationTimestamp
	@Column(name ="created_at")
	private Date createdAt;
	
	@UpdateTimestamp
	@Column(name ="updated_at")
	private Date updatedAt;
	
	@Column(name="created_by")
	private Integer createdBy;
	
	@Column(name="updated_by")
	private Integer updatedBy;
	
	
	@OneToMany(mappedBy = "permissionMaster")
	private List<RolePermission> rolePermisson;
	
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPermissionAction() {
		return permissionAction;
	}

	public void setPermissionAction(String permissionAction) {
		this.permissionAction = permissionAction;
	}

	public String getPermissionDesc() {
		return permissionDesc;
	}

	public void setPermissionDesc(String permissionDesc) {
		this.permissionDesc = permissionDesc;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public PermissionMaster() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public PermissionMaster(Integer id, String permissionAction, String permissionDesc, Date createdAt, Date updatedAt,
			Integer createdBy, Integer updatedBy) {
		super();
		this.id = id;
		this.permissionAction = permissionAction;
		this.permissionDesc = permissionDesc;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
	}

	public List<RolePermission> getRolePermisson() {
		return rolePermisson;
	}

	public void setRolePermisson(List<RolePermission> rolePermisson) {
		this.rolePermisson = rolePermisson;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	
	
}
