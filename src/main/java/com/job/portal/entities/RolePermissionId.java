package com.job.portal.entities;

import java.io.Serializable;
import java.util.Objects;

import jakarta.persistence.Embeddable;

@Embeddable
public class RolePermissionId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer roleId;
	
	private Integer permissionId;

	/**
	 * 
	 */
	public RolePermissionId() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param roleId
	 * @param permissionId
	 */
	public RolePermissionId(Integer roleId, Integer permissionId) {
		super();
		this.roleId = roleId;
		this.permissionId = permissionId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(permissionId, roleId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RolePermissionId other = (RolePermissionId) obj;
		return Objects.equals(permissionId, other.permissionId) && Objects.equals(roleId, other.roleId);
	}
	
	
	
	
	
}
