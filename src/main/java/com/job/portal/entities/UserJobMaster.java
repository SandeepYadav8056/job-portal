package com.job.portal.entities;


import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

import com.job.portal.enums.Status;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name="userJobMaster")
@Where(clause = "is_active = true")
public class UserJobMaster  implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer userJobId;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private Status status;
	
	@Column(name="application_date")
	private LocalDate applicationDate;
	
	@Column(name="is_active")
	private Boolean isActive = true;
	
	@CreationTimestamp
	private Date createdAt;

	@UpdateTimestamp
	private Date updateAt;
	
	@Column(name="created_by")
	private Integer createdBy;
	
	@Column(name ="updated_by")
	private Integer updatedBy;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private UserMaster userMaster;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "job_id")
	private JobMaster jobMaster;
	
	
	
	


	public UserJobMaster() {
		super();
		// TODO Auto-generated constructor stub
	}


	public UserJobMaster(Integer userJobId, Status status, LocalDate applicationDate, Boolean isActive, Date createdAt,
			Date updateAt, Integer createdBy, Integer updatedBy, UserMaster userMaster, JobMaster jobMaster) {
		super();
		this.userJobId = userJobId;
		this.status = status;
		this.applicationDate = applicationDate;
		this.isActive = isActive;
		this.createdAt = createdAt;
		this.updateAt = updateAt;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
		this.userMaster = userMaster;
		this.jobMaster = jobMaster;
	}


	public Integer getUserJobId() {
		return userJobId;
	}


	public void setUserJobId(Integer userJobId) {
		this.userJobId = userJobId;
	}


	public Status getStatus() {
		return status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}


	public LocalDate getApplicationDate() {
		return applicationDate;
	}


	public void setApplicationDate(LocalDate applicationDate) {
		this.applicationDate = applicationDate;
	}


	public Boolean getIsActive() {
		return isActive;
	}


	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}


	public Date getCreatedAt() {
		return createdAt;
	}


	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}


	public Date getUpdateAt() {
		return updateAt;
	}


	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}


	public Integer getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}


	public Integer getUpdatedBy() {
		return updatedBy;
	}


	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}


	public UserMaster getUserMaster() {
		return userMaster;
	}


	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}


	public JobMaster getJobMaster() {
		return jobMaster;
	}


	public void setJobMaster(JobMaster jobMaster) {
		this.jobMaster = jobMaster;
	}
	
	
	
	
	
	
	
	
	

	
	
	
}
