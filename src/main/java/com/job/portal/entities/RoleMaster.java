package com.job.portal.entities;

import java.util.Date;
import java.util.List;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name="roleMaster")
@Where(clause = "is_active = true")
public class RoleMaster {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "role_name")
	private String roleName;
	
	@Column(name="role_desc")
	private String roleDesc;
	
	@Column(name ="is_active")
	private Boolean isActive = true;
	
	@Column(name = "created_by")
	private Integer createdBy;
	
	@Column(name="updated_by")
	private Integer updatedBy;
	
	@Column(name="created_at")
	@CreationTimestamp
	private Date createdAt;
	
	@Column(name="updated_at")
	@UpdateTimestamp
	private Date updatedAt;
	
	@OneToMany(mappedBy ="roleMaster" , cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	private List<UserMaster> userMaster;
	
	@OneToMany(mappedBy = "roleMaster",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	private List<RolePermission> rolePermission;
	
	
	
	/**
	 * 
	 */
	public RoleMaster() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RoleMaster(Integer id, String roleName, String roleDesc, Integer createdBy, Integer updatedBy,
			Date createdAt, Date updatedAt, List<UserMaster> userMaster, List<RolePermission> rolePermission) {
		super();
		this.id = id;
		this.roleName = roleName;
		this.roleDesc = roleDesc;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.userMaster = userMaster;
		this.rolePermission = rolePermission;
	}
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleDesc() {
		return roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public List<UserMaster> getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(List<UserMaster> userMaster) {
		this.userMaster = userMaster;
	}

	public List<RolePermission> getRolePermission() {
		return rolePermission;
	}

	public void setRolePermission(List<RolePermission> rolePermission) {
		this.rolePermission = rolePermission;
	}

	
	
	
	
	

	

}
