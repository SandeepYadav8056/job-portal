package com.job.portal.entities;

import java.util.Date;

import com.job.portal.enums.MethodEnum;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "error_logger")
public class ErrorLogger {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="body",length = 10000)
	private String body;
	
	private String host;
	
	private MethodEnum method;
	
	@Column(length = 100000)
	private String message;
	
	private String url;
	
	private Date createdAt;

	public ErrorLogger() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ErrorLogger(Integer id, String body, String host, MethodEnum method, String message, String url,
			Date createdAt) {
		super();
		this.id = id;
		this.body = body;
		this.host = host;
		this.method = method;
		this.message = message;
		this.url = url;
		this.createdAt = createdAt;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public MethodEnum getMethod() {
		return method;
	}

	public void setMethod(MethodEnum method) {
		this.method = method;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	
	
	
	
	
}
