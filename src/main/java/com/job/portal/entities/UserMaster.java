package com.job.portal.entities;

import java.io.Serializable;
import java.util.List;

import org.hibernate.annotations.Where;

import com.job.portal.enums.Gender;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name="userMaster")
@Where(clause = "is_active = true")
public class UserMaster implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="full_name",length = 100)
	private String fullName;
	
	@Column(name="email",length = 100,unique = true)
	private String email;
	
	@Column(name="password",length = 150,nullable = false)
	private String password;
	
	@Column(name="gender")
	@Enumerated(EnumType.STRING)
	private Gender gender;
	
	private Boolean isActive = true;
	
	@OneToMany(mappedBy = "userMaster", cascade = {CascadeType.PERSIST,CascadeType.MERGE},fetch = FetchType.LAZY)
	private List<OtpEntity> otpEntity;
	
	
	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name = "role_id")
	private RoleMaster roleMaster;
	
	
	
	@OneToMany(mappedBy = "userMaster",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	private List<UserJobMaster> userJob; 
	
	@OneToMany(mappedBy = "userMaster",fetch = FetchType.LAZY)
	private List<JobMaster> jobMaster;
	
	
	
	public UserMaster() {
		super();
	}

	

	public UserMaster(Integer id, String fullName, String email, String password, Gender gender, Boolean isActive,
			List<OtpEntity> otpEntity, RoleMaster roleMaster, List<UserJobMaster> userJob, List<JobMaster> jobMaster) {
		super();
		this.id = id;
		this.fullName = fullName;
		this.email = email;
		this.password = password;
		this.gender = gender;
		this.isActive = isActive;
		this.otpEntity = otpEntity;
		this.roleMaster = roleMaster;
		this.userJob = userJob;
		this.jobMaster = jobMaster;
	}



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public List<OtpEntity> getOtpEntity() {
		return otpEntity;
	}

	public void setOtpEntity(List<OtpEntity> otpEntity) {
		this.otpEntity = otpEntity;
	}

	public RoleMaster getRoleMaster() {
		return roleMaster;
	}

	public void setRoleMaster(RoleMaster roleMaster) {
		this.roleMaster = roleMaster;
	}

	public List<UserJobMaster> getUserJob() {
		return userJob;
	}

	public void setUserJob(List<UserJobMaster> userJob) {
		this.userJob = userJob;
	}

	public List<JobMaster> getJobMaster() {
		return jobMaster;
	}

	public void setJobMaster(List<JobMaster> jobMaster) {
		this.jobMaster = jobMaster;
	}

	
	@Override
	public String toString() {
		return "{\"id\":" + id + ",\"name\":\"" + fullName + "\",\"email\":\"" + email + "\",\"password\":\"" + password
				+ "\"}";
	}


	
	
	
}
