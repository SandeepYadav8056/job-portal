package com.job.portal.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name="jobMaster")
@Where(clause = "is_active = true")
public class JobMaster implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="title",length = 150)
	private String title;
	
	@Column(name="description",length = 400)
	private String description;
	
	@Column(name="posted_date")
	private LocalDate postedDate;
	
	@CreationTimestamp
	private Date createdAt;
	
	@UpdateTimestamp 
	private Date updatedAt;
	
	private Integer updatedBy;
	
	private Integer createdBy;
	
	@Column(name ="is_active")
	private Boolean isActive = true;
	
	@OneToMany(mappedBy = "jobMaster",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	private List<UserJobMaster> userJobMaster;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private  UserMaster userMaster;

	/**
	 * 
	 */
	public JobMaster() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param id
	 * @param title
	 * @param description
	 * @param postedDate
	 * @param createdAt
	 * @param updatedAt
	 * @param isActive
	 * @param userJobMaster
	 */
	public JobMaster(Integer id, String title, String description, LocalDate postedDate, Date createdAt, Date updatedAt,
			Boolean isActive, List<UserJobMaster> userJobMaster) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.postedDate = postedDate;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.isActive = isActive;
		this.userJobMaster = userJobMaster;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDate getPostedDate() {
		return postedDate;
	}

	public void setPostedDate(LocalDate postedDate) {
		this.postedDate = postedDate;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public List<UserJobMaster> getUserJobMaster() {
		return userJobMaster;
	}

	public void setUserJobMaster(List<UserJobMaster> userJobMaster) {
		this.userJobMaster = userJobMaster;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	
	
	
	
	
}
