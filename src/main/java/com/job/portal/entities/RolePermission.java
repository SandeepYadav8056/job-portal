package com.job.portal.entities;

import java.util.Date;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.Table;

@Entity
@Table(name="role_permission")
@Where(clause = "is_active = true")
public class RolePermission {

	
	@EmbeddedId
	RolePermissionId rolePermissionId = new RolePermissionId();
	

	@ManyToOne(fetch = FetchType.LAZY,cascade = {CascadeType.MERGE,CascadeType.REFRESH,CascadeType.PERSIST})
	@MapsId("roleId")
	@JoinColumn(name = "role_id")
	private RoleMaster roleMaster;
	

	@ManyToOne(fetch = FetchType.LAZY,cascade = {CascadeType.MERGE,CascadeType.REFRESH,CascadeType.PERSIST})
	@MapsId("permissionId")
	@JoinColumn(name="permission_id")
	private PermissionMaster permissionMaster;
	
	
	@Column(name="is_active")
	private Boolean isActive = true;
	
	@CreationTimestamp
	private Date createdAt;
	
	
	@UpdateTimestamp
	private Date updatedAt;

	
	

	/**
	 * 
	 */
	public RolePermission() {
		super();
		// TODO Auto-generated constructor stub
	}


	public RolePermission(RolePermissionId rolePermissionId, RoleMaster roleMaster, PermissionMaster permissionMaster,
			Boolean isActive, Date createdAt, Date updatedAt) {
		super();
		this.rolePermissionId = rolePermissionId;
		this.roleMaster = roleMaster;
		this.permissionMaster = permissionMaster;
		this.isActive = isActive;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}


	public RolePermissionId getRolePermissionId() {
		return rolePermissionId;
	}


	public void setRolePermissionId(RolePermissionId rolePermissionId) {
		this.rolePermissionId = rolePermissionId;
	}


	public RoleMaster getRoleMaster() {
		return roleMaster;
	}


	public void setRoleMaster(RoleMaster roleMaster) {
		this.roleMaster = roleMaster;
	}


	public PermissionMaster getPermissionMaster() {
		return permissionMaster;
	}


	public void setPermissionMaster(PermissionMaster permissionMaster) {
		this.permissionMaster = permissionMaster;
	}


	public Boolean getIsActive() {
		return isActive;
	}


	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}


	public Date getCreatedAt() {
		return createdAt;
	}


	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}


	public Date getUpdatedAt() {
		return updatedAt;
	}


	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	
	
	
	
	

}
