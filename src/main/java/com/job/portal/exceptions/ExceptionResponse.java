package com.job.portal.exceptions;

public class ExceptionResponse {
	
	
	private  String errorMessage;
	
	private String requestedURL;

	public ExceptionResponse(String errorMessage, String requestedURL) {
		super();
		this.errorMessage = errorMessage;
		this.requestedURL = requestedURL;
	}

	public ExceptionResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getRequestedURL() {
		return requestedURL;
	}

	public void setRequestedURL(String requestedURL) {
		this.requestedURL = requestedURL;
	}
	
	

}
