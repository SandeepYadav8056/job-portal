package com.job.portal.exceptions;

import java.net.Inet4Address;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.job.portal.dtos.ErrorResponseDto;
import com.job.portal.entities.ErrorLogger;
import com.job.portal.enums.MethodEnum;
import com.job.portal.repositories.ErrorLoggerRepository;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.io.IOException;
import jakarta.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class GlobalExceptionHandler {
	
	@Autowired
	private ErrorLoggerRepository errorLoggerRepository;
	
	
	@ExceptionHandler(ResourceNotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public @ResponseBody ExceptionResponse handleResourceNotFoundException(ResourceNotFoundException ex,final HttpServletRequest request) {
		
		ExceptionResponse error = new ExceptionResponse();
		error.setErrorMessage(ex.getMessage());
		error.setRequestedURL(request.getRequestURI());
		return error;
	}
	
	
	@ExceptionHandler(AccessDeniedException.class)
	@ResponseStatus(value = HttpStatus.FORBIDDEN)
    public @ResponseBody ExceptionResponse handleAccessDeniedException(AccessDeniedException ex,final HttpServletRequest request) {
		
		ExceptionResponse error = new ExceptionResponse();
		error.setErrorMessage("Access Denied");
		error.setRequestedURL(request.getRequestURI());
		return error;
  
    }
	
	
	@ExceptionHandler()
	public ResponseEntity<?> handleMethodArgsNotValidException(MethodArgumentNotValidException ex){
		
		List<String> details = new ArrayList<>();
		
		ex.getBindingResult().getAllErrors().stream().forEach(error -> details.add(error.getDefaultMessage()));

		ErrorResponseDto error = new ErrorResponseDto();
		error.setMessage(details.get(0).split("\\*", 2)[0]);
		error.setMessageKey(details.get(0).split("\\*", 2)[1]);

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
		
	}
 	
	@ExceptionHandler(DataIntegrityViolationException.class)
	@ResponseStatus(value = HttpStatus.CONFLICT)
	public @ResponseBody ExceptionResponse handleDataIntegrityViolationException(
			final DataIntegrityViolationException exception) {

		ExceptionResponse error = new ExceptionResponse();
		error.setErrorMessage("Data is present already !!");
		error.setRequestedURL("Data_Redendency");
		return error;

	}

	
	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	@ResponseStatus(value = HttpStatus.METHOD_NOT_ALLOWED)
	public @ResponseBody ErrorResponseDto handleHttpRequestMethodNotSupportedException(
			final HttpRequestMethodNotSupportedException httpRequestMethodNotSupportedException) {

		ErrorResponseDto errorResponseDto = new ErrorResponseDto();
		errorResponseDto.setMessage("Method does not supported ");
		errorResponseDto.setMessageKey("Please check your method type");

		return errorResponseDto;

	}



	@ExceptionHandler(NoHandlerFoundException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public @ResponseBody ErrorResponseDto noHandlerFoundException(final NoHandlerFoundException exception) {

		ErrorResponseDto error = new ErrorResponseDto();
		error.setMessage("URL not Found, Please check URL");
		error.setMessageKey("URLNotFound");
		return error;

	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody ErrorResponseDto handleException(final Exception exception, HttpServletRequest request)
			throws IOException {
		ErrorLogger errorRequest = new ErrorLogger();
//		errorRequest.setBody(request instanceof StandardMultipartHttpServletRequest ? null
//				: request.getReader().lines().collect(Collectors.joining(System.lineSeparator())));
		errorRequest.setHost(Inet4Address.getLoopbackAddress().getHostAddress());
		errorRequest.setMessage(exception.getMessage());
		errorRequest.setMethod(MethodEnum.valueOf(MethodEnum.class, request.getMethod()));
		errorRequest.setUrl(request.getRequestURI());

		errorLoggerRepository.save(errorRequest);

		ErrorResponseDto error = new ErrorResponseDto();
		error.setMessage("Something went wrong. Please contact the administrator");
		error.setMessageKey("something went wrong.");
		return error;

	}

	@ExceptionHandler(MaxUploadSizeExceededException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public @ResponseBody ErrorResponseDto handledMaxUploadSizeExceededException(
			final MaxUploadSizeExceededException exceededException) {
		ErrorResponseDto error = new ErrorResponseDto();
		error.setMessage("Please decrease your file size");
		error.setMessageKey("File size should be below 20MB");
		return error;

	}

	@ExceptionHandler(ExpiredJwtException.class)
	@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
	public @ResponseBody ErrorResponseDto handleJwtExpiredException(final ExpiredJwtException jwtExpired) {
		ErrorResponseDto error = new ErrorResponseDto();
		error.setMessage("JWT expired");
		error.setMessageKey("jwtExpired");
		return error;
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ErrorResponseDto handleJsonErrors(HttpMessageNotReadableException exception) {
		ErrorResponseDto error = new ErrorResponseDto();
		error.setMessage("Please enter valid information");
		error.setMessageKey("Enter valid input ");
		return error;
	}

	@ExceptionHandler(NumberFormatException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ErrorResponseDto numberFormatExceptionHandler(NumberFormatException exception) {
		ErrorResponseDto error = new ErrorResponseDto();
		error.setMessage("Invalid  input");
		error.setMessageKey("Enter valid input");
		return error;
	}


	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ErrorResponseDto methodArgumentTypeMismatchException(MethodArgumentTypeMismatchException exception) {
		ErrorResponseDto error = new ErrorResponseDto();
		error.setMessage("Invalid uri");
		error.setMessageKey("Enter valid uri");
		return error;

	}

//	@ExceptionHandler(BindException.class)
//	public ErrorResponseDto handleBindException(BindException ex) {
//		List<String> details = new ArrayList<>();
//
//		for (ObjectError error : ex.get) {
//
//			details.add(error.getDefaultMessage());
//
//		}
//		ErrorResponseDto error = new ErrorResponseDto();
//		error.setMessage(details.get(0).split("\\*", 2)[0]);
//		error.setMessageKey(details.get(0).split("\\*", 2)[1]);
//
//		return error;
//
//	}

	
	
}