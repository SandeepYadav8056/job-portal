package com.job.portal.dto;

import com.job.portal.enums.Gender;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RegisterRequestDto {
	
	@NotEmpty(message="Fullname must not be blanked")
	private  String fullName;
	
	@NotBlank(message ="Email is required field")
	@NotEmpty(message ="Email is required field")
	private String email;
	
	@NotNull(message="Gender is required field")
	private Gender gender;
	
	@NotBlank(message ="Email is required field")
	@NotEmpty(message ="Email is required field")
	private String password;

}
