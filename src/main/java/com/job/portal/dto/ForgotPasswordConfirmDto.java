package com.job.portal.dto;

import com.job.portal.utils.ErrorMessageConstant;
import com.job.portal.utils.Validator;

import jakarta.validation.constraints.Pattern;

public class ForgotPasswordConfirmDto {

	@Pattern(regexp = Validator.PASSWORD_PATTERN , message = ErrorMessageConstant.INVALID_PASSWORD_FORMAT)
	private String password;
	
	private String otp;
	
	private String email;

	public ForgotPasswordConfirmDto(String password, String otp, String email) {
		super();
		this.password = password;
		this.otp = otp;
		this.email = email;
	}

	public ForgotPasswordConfirmDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
}
