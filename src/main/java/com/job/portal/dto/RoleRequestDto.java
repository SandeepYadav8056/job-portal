package com.job.portal.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RoleRequestDto {

	
	@NotEmpty(message ="RoleName must not Empty")
	@NotBlank(message = "RoleName must not blank")
	private String roleName;
	
	private String roleDesc;
	
	
}
