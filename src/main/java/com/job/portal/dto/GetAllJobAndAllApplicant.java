package com.job.portal.dto;

import java.time.LocalDate;
import java.util.List;

public class GetAllJobAndAllApplicant {

private  String title;
	
	private String desc;
	
	private LocalDate postedDate;
	
	private List<ApplicantDto> applicants;

	public GetAllJobAndAllApplicant() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GetAllJobAndAllApplicant(String title, String desc, LocalDate postedDate, List<ApplicantDto> applicants) {
		super();
		this.title = title;
		this.desc = desc;
		this.postedDate = postedDate;
		this.applicants = applicants;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public LocalDate getPostedDate() {
		return postedDate;
	}

	public void setPostedDate(LocalDate postedDate) {
		this.postedDate = postedDate;
	}

	public List<ApplicantDto> getApplicants() {
		return applicants;
	}

	public void setApplicants(List<ApplicantDto> applicants) {
		this.applicants = applicants;
	}
	
	
	
}
