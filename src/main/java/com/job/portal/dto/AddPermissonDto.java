package com.job.portal.dto;

public class AddPermissonDto {
	
	
	private String permissoinAction;
	
	private String permissonDesc;
	
	private String method;
	
	private String url;

	public AddPermissonDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AddPermissonDto(String permissoinAction, String permissonDesc, String method, String url) {
		super();
		this.permissoinAction = permissoinAction;
		this.permissonDesc = permissonDesc;
		this.method = method;
		this.url = url;
	}

	public String getPermissoinAction() {
		return permissoinAction;
	}

	public void setPermissoinAction(String permissoinAction) {
		this.permissoinAction = permissoinAction;
	}

	public String getPermissonDesc() {
		return permissonDesc;
	}

	public void setPermissonDesc(String permissonDesc) {
		this.permissonDesc = permissonDesc;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	
	

}
