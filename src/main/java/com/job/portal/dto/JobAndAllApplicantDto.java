package com.job.portal.dto;

import java.time.LocalDate;
import java.util.List;

import com.job.portal.idtos.JobApplicantDto;

public class JobAndAllApplicantDto {

	
	private  String title;
	
	private String desc;
	
	private LocalDate postedDate;
	
	
	private List<JobApplicantDto> applicants;


	public JobAndAllApplicantDto(String title, String desc, LocalDate postedDate, List<JobApplicantDto> applicants) {
		super();
		this.title = title;
		this.desc = desc;
		this.postedDate = postedDate;
		this.applicants = applicants;
	}


	public JobAndAllApplicantDto() {
		super();
		// TODO Auto-generated constructor stub
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getDesc() {
		return desc;
	}


	public void setDesc(String desc) {
		this.desc = desc;
	}


	public LocalDate getPostedDate() {
		return postedDate;
	}


	public void setPostedDate(LocalDate postedDate) {
		this.postedDate = postedDate;
	}


	public List<JobApplicantDto> getApplicants() {
		return applicants;
	}


	public void setApplicants(List<JobApplicantDto> applicants) {
		this.applicants = applicants;
	}
	
	
	
	
}
