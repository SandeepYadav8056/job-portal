package com.job.portal.dto;

public class ApplicantDto {

	
	private String fullName;
	
	private String gender;
	
	private  String appliedDate;
	
	private String status;
	
	private String email;

	public ApplicantDto(String fullName, String gender, String appliedDate, String status, String email) {
		super();
		this.fullName = fullName;
		this.gender = gender;
		this.appliedDate = appliedDate;
		this.status = status;
		this.email = email;
	}

	public ApplicantDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAppliedDate() {
		return appliedDate;
	}

	public void setAppliedDate(String appliedDate) {
		this.appliedDate = appliedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
	
	
	
	
}
