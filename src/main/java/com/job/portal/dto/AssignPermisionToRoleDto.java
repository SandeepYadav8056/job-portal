package com.job.portal.dto;

import java.util.List;

public class AssignPermisionToRoleDto {
	
	private List<Integer> permissionId;
	
	private Integer roleId;

	public AssignPermisionToRoleDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AssignPermisionToRoleDto(List<Integer> permissionId, Integer roleId) {
		super();
		this.permissionId = permissionId;
		this.roleId = roleId;
	}

	public List<Integer> getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(List<Integer> permissionId) {
		this.permissionId = permissionId;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	
	
	
	

}
