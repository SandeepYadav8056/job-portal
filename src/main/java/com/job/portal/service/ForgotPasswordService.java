package com.job.portal.service;

import java.time.LocalDateTime;

import com.job.portal.entities.UserMaster;

public interface ForgotPasswordService {

	public void saveOtp(int otp ,UserMaster user, LocalDateTime expiry);
}
