package com.job.portal.service;

import com.job.portal.dto.AssignPermisionToRoleDto;
import com.job.portal.dto.RoleRequestDto;

public interface RoleService {

	RoleRequestDto addRole(RoleRequestDto request);

	String assignPermissionToRole(AssignPermisionToRoleDto request, Integer userLoggedId);

	void deletePermissionFromRole();

}
