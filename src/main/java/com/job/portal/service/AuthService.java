package com.job.portal.service;

import java.util.ArrayList;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.job.portal.dto.ForgotPasswordConfirmDto;
import com.job.portal.dto.RegisterRequestDto;
import com.job.portal.entities.UserMaster;

import jakarta.mail.MessagingException;

public interface AuthService {

	public void registerCandidate(RegisterRequestDto request);
	
	public Boolean comparePassword(String password , String hashPassword);
	
	public ArrayList<String> getPermissionByUser(Integer userId);
	
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;

	public void generateOtpAndSendEmail(String email, UserMaster user) throws MessagingException;

	public String createForgotPasswordConfirm(ForgotPasswordConfirmDto forgotPasswordConfirmDto);
}
