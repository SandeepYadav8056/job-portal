package com.job.portal.service;

import com.job.portal.dto.AddPermissonDto;

import io.swagger.v3.oas.annotations.parameters.RequestBody;

public interface PermissionService {

	
	public Boolean addPermission( AddPermissonDto request,Integer loggedUserId);
	
	public String updatePermission(Integer id ,Integer userLoggedId,AddPermissonDto request);
	
	public String deletePermission(Integer id ,Integer userLoggedId );
	
}
