package com.job.portal.service;

import jakarta.mail.MessagingException;

public interface EmailService {

	
	public void SendSimpleMessage(String sendTo , String subject , String text ) throws MessagingException;
}
