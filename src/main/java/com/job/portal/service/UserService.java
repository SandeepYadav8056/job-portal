package com.job.portal.service;

import java.io.ByteArrayInputStream;

import org.springframework.data.domain.Page;

import com.job.portal.dto.RegisterRequestDto;
import com.job.portal.idtos.IJobListByUserDto;
import com.job.portal.idtos.IUserListDto;

import jakarta.servlet.http.HttpServletResponse;

public interface UserService {

	
	public RegisterRequestDto registerRecuritier( RegisterRequestDto request,Integer userLoggedId);

	public Page<IUserListDto> getAllUser(String pageNo, String pageSize);

	public Page<IUserListDto> getAllRecuriter(String pageNo, String pageSize);

	public Page<IJobListByUserDto> getAppliedJobByApplicant(Integer userId, String pageNo, String pageSize);

	public void deleteUserById(Integer id);

	public String updateUser(Integer id, RegisterRequestDto request);

	public ByteArrayInputStream exportCandidateExcel() throws Exception;
	

	public ByteArrayInputStream exportRecuriterExcel() throws Exception;

	public ByteArrayInputStream exportCandidateJobExcel() throws Exception;
}
