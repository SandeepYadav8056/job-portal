package com.job.portal.service;

import java.util.Date;
import java.util.function.Function;

import org.springframework.security.core.userdetails.UserDetails;

import io.jsonwebtoken.Claims;

public interface JwtTokenService {
	
	
	
	public String getUsernameFromToken(String token);
	
	public Date getExpirationDateFromToken(String token);
	
	public <T> T getClaimsFromToken(String token,Function<Claims, T> claimsresolver);
	
	public Claims getAllClaimsFromToken(String token);
	
	public Boolean isTokenExpired(String token);
	
	public String generateToken(UserDetails userDetails);
	
	public Boolean validateToken(String token , UserDetails userDetails);
	
	public String refreshToken(String token , UserDetails userDetails);
	
	public Date calculateExpirationDate(Date createDate);
	
	public  Boolean canTokenBeRefreshed(String token);
	
	public String getTokenType(String token) throws Exception;
	
	

}
