package com.job.portal.service;

import org.springframework.data.domain.Page;

import com.job.portal.dto.JobDto;
import com.job.portal.idtos.IGetAllJob;

public interface JobService {

	JobDto createJob(Integer loginId, JobDto request);

	String updateJob(Integer userLoggedId, JobDto request, Integer jobId);

	String deleteJobById(Integer jobId, Integer userLoggedId);

	Page<IGetAllJob> getAllJobs(String pageNo, String pageSize);

	Page<IGetAllJob> fetchJobByRecuritier(Integer userLoggedIn, String pageNo, String pageSize);

	
	

}
