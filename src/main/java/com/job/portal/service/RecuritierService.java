package com.job.portal.service;

import com.job.portal.dto.JobAndAllApplicantDto;
import com.job.portal.dtos.ListResponseDto;

public interface RecuritierService {

	ListResponseDto getAllApplicantOfAllJob(String pageNo, String pageSize, Integer userLoggedId);

	JobAndAllApplicantDto getJobByIdAndApplicants(Integer jobId);

}
