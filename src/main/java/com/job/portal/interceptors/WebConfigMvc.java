package com.job.portal.interceptors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfigMvc implements WebMvcConfigurer{
	
	
	@Autowired
	private ApiLoggerInterceptor apiLoggerInterceptor;
	
	@Autowired
	private AuthLoggerInterceptor authLoggerInterceptor;
	

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		
		registry.addInterceptor(apiLoggerInterceptor);
		
		registry.addInterceptor(authLoggerInterceptor);
		WebMvcConfigurer.super.addInterceptors(registry);
	}

	
	
}

