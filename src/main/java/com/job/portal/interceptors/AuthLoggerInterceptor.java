package com.job.portal.interceptors;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import com.job.portal.configurations.JwtTokenUtils;
import com.job.portal.entities.UserMaster;
import com.job.portal.repositories.UserMasterRepository;
import com.job.portal.utils.ApiUrls;
import com.job.portal.utils.Constant;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
public class AuthLoggerInterceptor implements HandlerInterceptor {
	
	

	
	public AuthLoggerInterceptor() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Autowired
	private JwtTokenUtils jwtTokenUtils;
	
	@Autowired
	private UserMasterRepository userRepository;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		
		String authHeader = request.getHeader(Constant.AUTHORIZATION);
		String token = (null != authHeader) ? authHeader.split(" ")[1] : null;
		
		ArrayList<String> urlsWithoutHeader = new ArrayList<>(Arrays.asList(ApiUrls.URLS_WITHOUT_HEADER));
		String requestUrl = request.getRequestURI();
		
		if(null != token && !urlsWithoutHeader.contains(requestUrl)) {
			
			final String email = jwtTokenUtils.getUsernameFromToken(token);
			UserMaster userMaster = userRepository.findByEmailIgnoreCaseAndIsActiveTrue(email).get();
			
			request.setAttribute("X-user-id", userMaster.getId());
			request.setAttribute("X-user-email", email);
		}
		return HandlerInterceptor.super.preHandle(request, response, handler);
	}

	
	
	
}
