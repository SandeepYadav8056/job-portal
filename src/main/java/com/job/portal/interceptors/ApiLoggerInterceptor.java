package com.job.portal.interceptors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;
import org.springframework.web.servlet.HandlerInterceptor;

import com.job.portal.entities.ApiLogger;
import com.job.portal.service.ApiLoggerService;
import com.job.portal.utils.ApiUrls;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
public class ApiLoggerInterceptor  implements HandlerInterceptor{

	@Autowired
	private ApiLoggerService apiLoggerService;
	
	
	
	public ApiLoggerInterceptor() {
		super();
	}



	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		// TODO Auto-generated method stub
		
		
	ArrayList<String> urlsWithoutHeader = new ArrayList<>(Arrays.asList(ApiUrls.URLS_WITHOUT_HEADER) );
	
	final String requestUrl = request.getRequestURI();
	
	
	if(!urlsWithoutHeader.contains(requestUrl)) {

		ApiLogger apiDetails = new ApiLogger();
		
		apiDetails.setIpAddress(request.getRemoteAddr());
		apiDetails.setHost(request.getRemoteHost());
		apiDetails.setMethod(request.getMethod());
		apiDetails.setUrl(requestUrl);
		apiDetails.setBody(request instanceof StandardMultipartHttpServletRequest ? null : request.getReader().lines().collect(Collectors.joining(System.lineSeparator())));
		
		apiLoggerService.createApiLogger(apiDetails);
		return true;
	}
	
		return HandlerInterceptor.super.preHandle(request, response, handler);
	}

	
	
}
