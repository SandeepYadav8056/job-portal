package com.job.portal.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.job.portal.entities.RoleMaster;

public interface RoleMasterRepository extends JpaRepository<RoleMaster, Integer> {

	Optional<RoleMaster> findByRoleNameAndIsActiveTrue(String string);

	RoleMaster findByIdAndIsActiveTrue(Integer roleId);

}
