package com.job.portal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.job.portal.entities.ErrorLogger;

public interface ErrorRepository extends JpaRepository<ErrorLogger, Integer> {

}
