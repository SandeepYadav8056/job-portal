package com.job.portal.repositories;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.job.portal.entities.PermissionMaster;

import io.lettuce.core.dynamic.annotation.Param;

public interface PermissionRepository extends JpaRepository<PermissionMaster, Integer>{

	PermissionMaster findByIdAndIsActiveTrue(Integer id);

	 @Modifying
	 @Query(value = "UPDATE permission_master p SET p.is_active = false, p.updated_by = :userLoggedId WHERE p.id = :id", nativeQuery = true)
	 void deletePermissionById(@Param("id") Integer id, @Param("userLoggedId") Integer userLoggedId);
	
	@Query(value = "SELECT p FROM PermissionMaster p WHERE p.id IN :permissionId AND p.isActive = true")
    List<PermissionMaster> findAllByIdAndIsActiveTrue(@Param("permissionId") List<Integer> permissionId);

	
	@Query(value="SELECT p.permission_action FROM user_master u\r\n"
			+ "JOIN role_master r ON u.role_id = r.id\r\n"
			+ "JOIN role_permission rp ON r.id = rp.role_id\r\n"
			+ "JOIN permission_master p ON rp.permission_id = p.id\r\n"
			+ "WHERE u.id =:userId AND u.is_active = true AND p.is_active = true AND rp.is_active = true\r\n"
			+ "",nativeQuery = true)
	ArrayList<String> findPermissionByUser(@Param("userId") Integer userId);
}
 