package com.job.portal.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.job.portal.entities.JobMaster;
import com.job.portal.idtos.IGetAllJob;

import jakarta.transaction.Transactional;

public interface JobMasterRepository  extends JpaRepository<JobMaster, Integer>{

	Optional<JobMaster> findByIdAndIsActiveTrue(Integer jobId);

	

	@Transactional
	@Modifying
	@Query(value="UPDATE job_master  SET is_active = false ,updated_by =:userLoggedId WHERE id =:jobId ",nativeQuery = true)
	void deleteJob(@Param("jobId") Integer jobId,@Param("userLoggedId") Integer userLoggedId);



	@Query(value="SELECT id as id ,title as jobTitle , description as jobDesc ,posted_date as PostedDate FROM job_master WHERE is_active = true order by id desc",nativeQuery =  true )
	Page<IGetAllJob> fetAllJob(Pageable pageable);


	@Query(value="SELECT j.id as Id , j.title as jobTitle , j.description as jobDesc ,posted_date as PostedDate FROM job_master j \r\n"
			+ "LEFT JOIN  user_master u ON j.user_id = u.id \r\n"
			+ "WHERE j.user_id =:userLoggedId and u.is_active = true and j.is_active = true order by j.id desc",nativeQuery = true)
	Page<IGetAllJob> fetchJobByRecuritier(@Param("userLoggedId") Integer userLoggedId, Pageable pageable);


	@Query(nativeQuery = true,
			value="SELECT EXISTS (   SELECT 1 FROM job_master WHERE title = :title AND user_id = :loginId ) AS job_exists")
	Boolean findByJobTitleAndRecruiter(String title, Integer loginId);


	@Query(nativeQuery = true,
			value="SELECT EXISTS (  SELECT 1 FROM job_master WHERE id = :id AND user_id = :loginId ) AS job_exists")
	Boolean findByJobIdAndRecruiter(Integer id, Integer loginId);



	@Query(nativeQuery = true, 
			value="SELECT  CASE WHEN EXISTS (\r\n"
					+ "        SELECT * \r\n"
					+ "        FROM user_job_master userjob \r\n"
					+ "        WHERE job_id = 9 AND user_id = 1\r\n"
					+ "    ) THEN 'true' ELSE 'false' END AS result;")
	Boolean alreadyAppliedforJob(Integer jobId, Integer userLoggedId);

	

}
