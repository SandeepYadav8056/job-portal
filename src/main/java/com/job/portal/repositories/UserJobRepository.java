package com.job.portal.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.job.portal.entities.UserJobMaster;
import com.job.portal.idtos.IJobListDto;
import com.job.portal.idtos.JobApplicantDto;

public interface UserJobRepository extends JpaRepository<UserJobMaster, Integer> {

	
	
	@Query(value="SELECT \r\n"
			+ "		   job.title AS Title,\r\n"
			+ "		    job.description AS Description,\r\n"
			+ "			job.posted_date AS PostedDate,\r\n"
			+ "		    JSON_AGG(\r\n"
			+ "		        json_build_object(\r\n"
			+ "					'fullName', users.full_name, \r\n"
			+ "					'email', users.email ,\r\n"
			+ "					'appliedDate', userjob.application_date,\r\n"
			+ "					'status', userjob.status\r\n"
			+ "		)\r\n"
			+ "		    ) AS Applicants\r\n"
			+ "		FROM \r\n"
			+ "		   job_master job\r\n"
			+ "		LEFT JOIN \r\n"
			+ "		    user_job_master userjob ON job.id = userjob.job_id\r\n"
			+ "		LEFT JOIN \r\n"
			+ "		    user_master users ON userjob.user_id = users.id\r\n"
			+ "		WHERE  \r\n"
			+ "		    userjob.user_job_id IS NOT NULL And job.user_id =:userLoggedId  And job.is_active = true And userjob.is_active = true And users.is_active = true \r\n"
			+ "		GROUP BY \r\n"
			+ "		    job.title, job.description, job.posted_date ",nativeQuery = true)
	Page<IJobListDto> getAllApplicantOfAllJob(Pageable pageable,@Param("userLoggedId") Integer userLoggedId);

	//fullName, String gender, String appliedDate, String status, String emai
	
	@Query(value = "SELECT new com.job.portal.idtos.JobApplicantDto(users.fullName, userJob.applicationDate ,users.email, userJob.status,users.gender) " +
            "FROM UserJobMaster userJob " +
            "LEFT JOIN userJob.userMaster users " +
            "LEFT JOIN userJob.jobMaster job " +
            "WHERE job.id = :jobId AND job.isActive = true " +
            "AND userJob.isActive = true AND users.isActive = true " +
            "AND userJob.userJobId IS NOT NULL " +
            "ORDER BY userJob.userJobId DESC")
	List<JobApplicantDto> getJobAllAplicant(@Param("jobId") Integer jobId);

	
	/*
	 * 

SELECT j.id ,j.title , userjob.application_date ,userjob.status FROM user_master u
INNER JOIN user_job_master userjob ON u.id = userjob.user_id
INNER JOIN job_master j ON userjob.job_id = j.id 
WHERE u.id = 1 and u.is_active = true and userjob.is_active = true and j.is_active = true

	 */

}
