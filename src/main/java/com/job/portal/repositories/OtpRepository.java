package com.job.portal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.job.portal.entities.OtpEntity;

import jakarta.transaction.Transactional;

public interface OtpRepository extends JpaRepository<OtpEntity, Integer>{

	
	@Modifying
	@Transactional
	@Query(value="DELETE FROM  otp_logger WHERE user_id =:id ",nativeQuery= true)
	void deleteAllByUserId(Integer id);

	
	@Query(nativeQuery =  true , value="SELECT * FROM otp_logger u WHERE u.otp = :otp ")
	OtpEntity findbyOtp(@Param("otp") Integer otp);
	

}
