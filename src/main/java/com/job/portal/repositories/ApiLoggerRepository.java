package com.job.portal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.job.portal.entities.ApiLogger;

public interface ApiLoggerRepository extends JpaRepository<ApiLogger, Integer>{

}
