package com.job.portal.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.job.portal.entities.UserMaster;
import com.job.portal.idtos.ICandidateJobExportDto;
import com.job.portal.idtos.IJobListByUserDto;
import com.job.portal.idtos.IUserListDto;

import jakarta.transaction.Transactional;

public interface  UserMasterRepository extends JpaRepository<UserMaster, Integer>{

	UserMaster findByIdAndIsActiveTrue(Integer loginId);

	Optional<UserMaster> findByEmailIgnoreCaseAndIsActiveTrue(String email);

	
	@Query(nativeQuery = true , 
			value="SELECT u.id ,u.full_name as Name , u.email as email ,u.gender as gender FROM user_master u \r\n"
				+ "INNER JOIN role_master r ON u.role_id = r.id \r\n"
				+ "WHERE r.role_name = 'Candidate' and u.is_active = true ORDER BY u.id DESC")
	Page<IUserListDto> getAllCandidate(Pageable pageable);

	
	@Query(nativeQuery = true,
			value="SELECT u.id ,u.full_name as Name, 	 u.email as email ,u.gender as gender  FROM user_master u \r\n"
					+ "INNER JOIN role_master r ON u.role_id = r.id \r\n"
					+ "WHERE r.role_name = 'Recuriter' and u.is_active = true ORDER BY u.id DESC"
			)
	Page<IUserListDto> getAllRecuriter(Pageable pageable);

	
	
	@Query(nativeQuery = true, 
			value="SELECT j.id as jobId ,j.title as title , userjob.application_date as ApplicationDate  ,userjob.status as Status FROM user_master u \r\n"
			+ "INNER JOIN user_job_master userjob ON u.id = userjob.user_id \r\n"
			+ "INNER JOIN job_master j ON userjob.job_id = j.id \r\n"
			+ "WHERE u.id =:userId and u.is_active = true and userjob.is_active = true and j.is_active = true ORDER BY userjob.user_job_id DESC \r\n"
			)
	Page<IJobListByUserDto> getAppliedJobByCandidate(@Param("userId") Integer userId, Pageable pageable);

	@Override
	@Transactional
	@Modifying
	@Query(nativeQuery = true,
			value="UPDATE user_master  set is_active = false  WHERE id =:id")
	void deleteById(@Param("id") Integer id);

	
	@Query(nativeQuery = true,
			value ="SELECT u.full_name as name , u.email as email , u.gender as gender , j.title as jobTitle , uj.application_date as applicationDate ,uj.status as status FROM user_master u \r\n"
					+ "INNER JOIN user_job_master uj ON u.id = uj.user_id\r\n"
					+ "INNER JOIN job_master j ON uj.job_id = j.id\r\n"
					+ "WHERE u.is_active = true AND uj.is_active = true AND j.is_active = true " )
	List<ICandidateJobExportDto> getAllCandidateJob();
	
	
	

		
	

}
