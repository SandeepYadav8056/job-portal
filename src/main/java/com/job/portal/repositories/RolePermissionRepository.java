package com.job.portal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.job.portal.entities.RolePermission;
import com.job.portal.entities.RolePermissionId;

public interface RolePermissionRepository  extends JpaRepository<RolePermission, RolePermissionId>{

}
