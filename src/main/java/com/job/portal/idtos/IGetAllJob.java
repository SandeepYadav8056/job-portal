package com.job.portal.idtos;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

public interface IGetAllJob {

	
	public Integer getId();
	
	public String getjobTitle();
	
	public String getJobDesc();
	
	@JsonFormat(pattern = "dd-MMM-yyyy")
	public LocalDate getPostedDate();
	
	
}
