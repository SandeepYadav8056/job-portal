package com.job.portal.idtos;

import java.time.LocalDate;

import com.job.portal.enums.Status;

public interface IJobListByUserDto {
	
	public Integer getJobId();
	
	public String getTitle();
	
	public LocalDate getApplicationDate();
	
	public Status getStatus();
	

}
