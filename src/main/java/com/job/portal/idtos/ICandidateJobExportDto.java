package com.job.portal.idtos;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.job.portal.enums.Gender;
import com.job.portal.enums.Status;

public interface ICandidateJobExportDto {
	
	
	public String getName();
	
	public String getEmail();
	
	public Gender getGender();
	
	public String getJobTitle();
	
	@JsonFormat(pattern = "dd-MM-yyyy")
	public LocalDate getApplicationDate();
	
	public Status getStatus();
	
	

}
