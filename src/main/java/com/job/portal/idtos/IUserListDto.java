package com.job.portal.idtos;

import com.job.portal.enums.Gender;

public interface IUserListDto {
	
	
	public Integer getId();
	
	public String getName();
	
	public String getEmail();
	
	public Gender getGender();
	

}
