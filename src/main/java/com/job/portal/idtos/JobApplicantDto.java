package com.job.portal.idtos;

import java.time.LocalDate;

import com.job.portal.enums.Gender;
import com.job.portal.enums.Status;


public class JobApplicantDto {
	
	
	private String  fullName;
	
	private LocalDate applicationDate;
	
	private String email;
	
	private  Status status;
	
	private Gender gender;

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public LocalDate getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(LocalDate applicationDate) {
		this.applicationDate = applicationDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public JobApplicantDto(String fullName, LocalDate applicationDate, String email, Status status, Gender gender) {
		super();
		this.fullName = fullName;
		this.applicationDate = applicationDate;
		this.email = email;
		this.status = status;
		this.gender = gender;
	}

	public JobApplicantDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	

}
