package com.job.portal.idtos;

import java.time.LocalDate;
import java.util.List;

public interface IJobListDto {
	
	
	public String getTitle();
	
	public String getDescription();
	
	public LocalDate getPostedDate();
	
	public String getApplicants();
	
	
	

}
